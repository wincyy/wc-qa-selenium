#Setting up Python 3.x and required dependencies for local functional Selenium test development

The following guide assumes you are using macOS 10.13 (High Sierra) or higher. If you are on a Windows or *nix based system, follow appropriate instructions to install Python 3.6+ for your operating system.

By default, macOS comes with Python 2.7.x installed. You should not remove or interfere with this installation, as it is used for a number of critical system tasks.

The easiest way to install Python 3.6+ is by using HomeBrew:

 https://brew.sh
 
 Open a terminal, type `brew -v` to check if you already have HomeBrew installed. If not, follow the instructions on the site to install it - which as of the time of this writing, can be done by running the following in the terminal:
 
 ```
 /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
 ```
 
 Once HomeBrew is installed, you can simply install Python 3.x with the following command:
 
 ```
 brew install python
 ```
 
 Which, as of the time of this writing, should be Python 3.7.x (all BiblioCommons Selenium tests should work perfectly fine with both Python 3.6 and 3.7).
 
 Once Python 3.x is installed, you will now have both a `python` and `python3` alias available in your terminal:
 
    python -V
    Python 2.7.10
    
    python3 -V
    Python 3.7.1
 
 Please note that from this point forward, all documentation will refer to `python3`.
 
 Now, change directory to the root of the `bc-qa-selenium` project and run the following:
 
 ```
 pip3 install -r requirements.txt
 ```
 
 This looks at the list of dependencies in the `requirements.txt` file of the project and installs/updates Python packages accordingly. As we add new dependencies to the project, you should re-run this command to install them on your local system.
 
 Please note that you should also have a JRE or JDK installed, in order to be able to run the Selenium Server JAR file locally. Additionally, you should also install `wget`, which can again be done via HomeBrew:
 
 ```
 brew install wget
 ```
 
 That's it! You are now all set up and ready to run some Selenium functional tests - instructions to do so are available in the execution.md document within this project folder.
 