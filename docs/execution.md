#Running Selenium functional tests on your local system post-Python 3.x (+ dependencies) setup

By default, the `bc-qa-selenium` project is set up to run tests using Google Chrome on the local system - this behaviour is defined in the `conftest.py` file, which can be adjusted to run the tests in other browsers as well as on remote systems (or cloud providers like Gridlastic, Sauce Labs, etc).

To achieve this, we use an instance of the RemoteDriver object in Selenium, to instantiate and drive our desired browser. Contrary to what the name may imply, a RemoteDriver instance can be executed on your local machine, as long as the `HUB` parameter is set to localhost.

There are multiple "moving" parts in the browser instantiation: the selenium-webdriver Python module, the Selenium Server JAR file, the appropriate browser driver executable (e.g ChromeDriver, GeckoDriver, etc) and the browser itself. To alleviate the complexity of having to deal with various components, an easy to use `start-selenium-server` shell script is provided in the `development` folder of the `bc-qa-selenium` project. Open a terminal (or a new terminal tab) to the root of the project and run the following command:

```
./development/start-selenium-server.sh
``` 

The first time you run this shell script, it will create the folder path `selenium\drivers` in your Home folder and download and extract specific versions of the ChromeDriver and GeckoDriver executables that are used to drive Chrome and Firefox respectively. As our project depency versions are updated, this shell script will be updated to reflect that and simply deleting the `selenium` folder and re-running the script will grab all latest versions of the files required.


Once the driver files are downloaded and ready to use, the script will launch an instance of the Selenium Server. You'll see output resembling the following:

```
[The Selenium Hub is available at: http://localhost:4444/wd/hub/static/resource/hub.html]

12:41:46.186 INFO [GridLauncherV3.parse] - Selenium server version: 3.141.59, revision: e82be7d358
12:41:46.318 INFO [GridLauncherV3.lambda$buildLaunchers$3] - Launching a standalone Selenium Server on port 4444
2019-02-04 12:41:46.428:INFO::main: Logging initialized @884ms to org.seleniumhq.jetty9.util.log.StdErrLog
12:41:46.759 INFO [WebDriverServlet.<init>] - Initialising WebDriverServlet
12:41:46.881 INFO [SeleniumServer.boot] - Selenium Server is up and running on port 4444
```

This process should be left running whenever you want to run tests. You can kill the process with the `Ctrl + C` shortcut.

You can also monitor the status of your local Selenium Server by opening a browser to: 

http://localhost:4444/wd/hub/static/resource/hub.html

Note that running the `start-selenium-server` shell script subsequent times without any version changes, will simply directly start the server, with no need to download and extract driver executables again.

In another terminal window or tab, cd to the root of `bc-qa-selenium`. You can run a single test like this:

```
pytest tests/functional/<product_folder>/test_<filename>.py --alluredir=allure-results
```

For example:

```
pytest tests/functional/core/test_C45995.py --alluredir=allure-results
```

If you want to run all tests for a given BiblioCommons product, e.g all Core tests, simply provide the correct path with no file name:

```
pytest tests/functional/core --alluredir=allure-results
```

You can also use PyTest "marks", which are custom test annotations used to filter our test runs, e.g:

```
pytest -m analytics --alluredir=allure-results
```

Will run _only_ tests marked with `@pytest.mark.analytics` in the test body. Note that you can also use the inverse:

```
pytest -m "not analytics" --alluredir=allure-results
```

To run all tests _except_ the ones marked with `@pytest.mark.analytics`.

It's also possible to combine folder paths and marks, like so:

```
pytest tests/functional/core -m demo --alluredir=allure-results
```

This will run tests found in the `core` folder only, that are also marked with the `@pytest.mark.demo` annotation (temporary mark used to filter tests that currently only run on our Demo environment).

As marks/annotations are currently WIP, a finalized list of valid marks will be added to this document over time.

Additional documentation on PyTest test execution/filters, etc is available at:

https://pytest.readthedocs.io/en/latest/
