from mimesis import Numbers
from datetime import *
from dateutil.relativedelta import relativedelta
from random import choice, randint
import uuid
import requests
# from configuration.webdriver import WebDriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import configuration.system

class TestConnector():
    class User():

        def __init__(self, barcode = None, adult = True):
            if barcode is None:
                self.barcode = str(uuid.uuid4()).replace('-', '')[:19]
                # print(id)
                # self.barcode = id[:19]
            else:
                self.barcode = barcode
            self.pin = ''.join(str(digit) for digit in Numbers().integers(start = 0, end = 9, length = 6))
            now = datetime.now()
            if adult:
                years = randint(13, 85)
                months = randint(0, 11)
            else:
                years = randint(0, 12)
                months = randint(0, 10)
            birthday = (now - relativedelta(years = years, months = months, days = randint(1, 31)))
            self.age_in_years = relativedelta(now, birthday).years
            self.dob = birthday.strftime("%Y-%m-%d")
            self.name = ''

        def create(self, driver):
            pin_signals = ['$h0', '$c0', '$f0']
            if self.dob:
                dob = '$p1b' + self.dob
                pin_signals.append(dob + '&e2019-11-17')

            if pin_signals:
                signals = ''.join(pin_signals)
                create_account_URL = "{}/account/{}/{}{}".format(configuration.system.test_connector_endpoint, self.barcode, self.pin, signals)
            else:
                create_account_URL = "{}/account/{}/{}".format(configuration.system.test_connector_endpoint, self.barcode, self.pin)

            response = requests.get(create_account_URL)
            if response.status_code == 200:
                # chrome_options = Options()
                # chrome_options.add_argument('--headless')
                # browser = webdriver.Remote(configuration.system.webdriver_hub, desired_capabilities = webdriver.DesiredCapabilities.CHROME)
                browser = driver # WebDriver().instance
                # browser = webdriver.Chrome(executable_path = configuration.chrome.driver_path, chrome_options = chrome_options)
                browser.get("{}/user/login?destination=%2F".format(configuration.system.base_url))
                browser.find_element_by_name('name').send_keys(self.barcode)
                browser.find_element_by_name('user_pin').send_keys(self.pin)
                browser.find_element_by_name('commit').click()

                WebDriverWait(browser, 5).until(EC.element_to_be_clickable((By.ID, 'registration_linking_option_normal')))
                WebDriverWait(browser, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "[testid='registration_next_step']")))
                browser.find_element_by_css_selector("[testid='registration_next_step']").click()

                WebDriverWait(browser, 5).until(EC.visibility_of_element_located((By.ID, 'user_email')))
                WebDriverWait(browser, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "[testid='registration_next_step']")))
                browser.find_element_by_css_selector("[testid='registration_next_step']").click()

                if self.age_in_years >= 13: # Adult
                    WebDriverWait(browser, 5).until(EC.visibility_of_element_located((By.ID, 'user_name')))
                    browser.find_element_by_id('user_name').send_keys("Test_" + self.barcode)
                else:
                    WebDriverWait(browser, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "[data-test-id='user-name-color-select']")))
                    browser.find_element_by_css_selector("div[data-test-id='user-name-color-select']").click()
                    browser.find_element_by_css_selector("div[data-test-id='user-name-color-select'] li[data-original-index='2'] > a > span").click()
                    browser.find_element_by_css_selector("div[data-test-id='user-name-animal-select']").click()
                    browser.find_element_by_css_selector("div[data-test-id='user-name-animal-select'] li[data-original-index='2'] > a > span").click()
                    # During the selection of Color/Animal, the spinner can break the 'Continue' button flow so we have to wait for it
                    # to be out of the way:
                    WebDriverWait(browser, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "[data-test-id='active-spinner']")))
                    WebDriverWait(browser, 5).until(EC.invisibility_of_element_located((By.CSS_SELECTOR, "[data-test-id='active-spinner']")))

                browser.find_element_by_class_name('cp_pretty_checkbox').click() # 'Accept Terms' checkbox
                accept_terms = browser.find_element_by_css_selector("[testid='checkbox_accept_terms']")
                WebDriverWait(browser, 5).until(EC.element_to_be_selected(accept_terms))
                WebDriverWait(browser, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "[data-step='finish']")))
                browser.find_element_by_css_selector("[data-step='finish']").click()
                WebDriverWait(browser, 10).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, "[data-test-id='registration_header']"), "Your account is now set up!"))
                browser.delete_all_cookies()
                # browser.quit()


                # print('{:<10s}{:>8s}{:>24s}{:>13s}{:>19s}'.format(self.barcode, self.pin, "Test_" + self.barcode, self.dob, " ({} year(s) old)".format(self.age_in_years)))
                return self
            else:
                raise(RequestException, "The request did not return HTTP 200 - the user account was NOT created.")
