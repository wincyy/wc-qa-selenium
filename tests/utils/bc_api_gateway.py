import requests
import re
import json
from collections import namedtuple
from pprint import pprint

class NoMatchForQueryError(Exception):
   """Raised when the query results in no matches found"""
   pass

class APIGateway():
    def __init__(self, base_url):
        # API Gateway requests require the environment and library components from
        # the URL, e.g "demo.bibliocommons.com" and "chipublib", so we split the
        # Base URL here and return the separate values:
        removed_scheme = base_url.replace('https://', '')
        split_url = removed_scheme.split('.', 1)

        self.env = split_url[1]
        self.library = split_url[0]

class BCAPIGatewayBibs(APIGateway):
    def search(
        self,
        term,
        search_type = "keyword",
        format = "BK",
        type = "PHYSICAL",
        provider = "ILS",
        available = 1,
        holds = 0
        ):

        # Initialize an empty list to hold query matches:
        items = []

        for i in range(1, 3):
            # print('https://gateway.{}/v2/libraries/{}/bibs/search?query={}&searchType={}&f_FORMAT={}&page={}'.format(self.env, self.library, term, search_type, format, i))
            response = requests.get('https://gateway.{}/v2/libraries/{}/bibs/search?query={}&searchType={}&f_FORMAT={}&page={}'.format(self.env, self.library, term, search_type, format, i))
            body = response.json()
            bibs = body["entities"]["bibs"]

            for bib in bibs:
                Availability = namedtuple("Availability", "total available on_hold")
                Bib = namedtuple("Bib", "availability format type provider title subtitle eresourceURL id")
                Bib.__new__.__defaults__ = (None, None, None, None, None, None, None, None)

                # Populate availability details:
                if bibs[bib]["availability"]["totalCopies"] is not None:
                    total_copies = int(bibs[bib]["availability"]["totalCopies"])
                else:
                    total_copies = 0

                if bibs[bib]["availability"]["availableCopies"] is not None:
                    available_copies = int(bibs[bib]["availability"]["availableCopies"])
                else:
                    available_copies = 0

                if bibs[bib]["availability"]["heldCopies"] is not None:
                    held_copies = int(bibs[bib]["availability"]["heldCopies"])
                else:
                    held_copies = 0
                availability = Availability(total = total_copies, available = available_copies, on_hold = held_copies)
                # Format item ID:
                split_id = re.split("S|C", bib)
                split_id.pop(0)
                # Populate item details:
                item = Bib(
                    availability = availability,
                    format = bibs[bib]["briefInfo"]["format"],
                    type = bibs[bib]["availability"]["bibType"],
                    provider = bibs[bib]["policy"]["provider"],
                    title = bibs[bib]["briefInfo"]["title"],
                    subtitle = bibs[bib]["briefInfo"]["subtitle"],
                    eresourceURL = bibs[bib]["availability"]["eresourceUrl"],
                    id = split_id[1] + split_id[0].zfill(3)
                    )

                if item.availability.available >= available and item.availability.on_hold >= holds:
                    items.append(item)
            # Raise an error if no matching results are found:
            if len(items) == 0:
                print("No results found for the given query parameters:")
                parameters = {
                    "Search Term": term,
                    "Search Type": search_type,
                    "Item Format": format,
                    "Item Provider": provider,
                    "Available Copies": available,
                    "On Hold Copies": holds
                }
                pprint(parameters)
                print(50 * "-")
                raise NoMatchForQueryError

        return items
