import pytest
import allure
import sure
import sys
sys.path.append('tests')

from utils.bc_api_gateway import BCAPIGatewayBibs
from utils.bc_data_layer import DataLayer
from pages.core.v2.search_results import SearchResultsPage
from pages.core.home import HomePage

@pytest.fixture(scope='class')
def confirm_and_cancel_hold(request, selenium_setup_and_teardown):
    driver = request.cls.driver
    request.cls.datalayer = DataLayer(driver)
    RESULT_INDEX = 0
    GROUP_INDEX = 0
    base_url = "https://chipublib.demo.bibliocommons.com"
    home_page = HomePage(driver, base_url).open()
    home_page.header.log_in('cpltest2', '60605')
    bibs = BCAPIGatewayBibs(base_url).search(term='c')
    holdable_bib = next(bib for bib in bibs if
                        bib.availability.available >= 1 and bib.availability.on_hold < bib.availability.available)
    home_page.header.search_for(holdable_bib.title)
    search_page = SearchResultsPage(driver)
    search_page.search_result_items[RESULT_INDEX].grouped_search_items[GROUP_INDEX].place_hold.click()
    # search_page.search_result_items[RESULT_INDEX].wait.until(
    #     lambda s: search_page.search_result_items[RESULT_INDEX].is_confirm_hold_displayed)
    # search_page.search_result_items[RESULT_INDEX].confirm_hold.click()
    search_page.wait.until(lambda s: search_page.search_result_items[GROUP_INDEX].is_alert_displayed)
    search_page.search_result_items[RESULT_INDEX].grouped_search_items[GROUP_INDEX].cancel_hold.click()
    search_page.search_result_items[RESULT_INDEX].confirm_cancel_hold.click()
    search_page.wait.until(lambda s: search_page.search_result_items[GROUP_INDEX].is_alert_displayed)
    search_page.wait.until(
        lambda s: search_page.search_result_items[RESULT_INDEX].is_place_hold_displayed)

@pytest.mark.analytics
@pytest.mark.usefixtures('confirm_and_cancel_hold')
class TestAnalyticsHolds:
    @allure.title("Analytics: Holds: placing a hold pushes event to dataLayer with expected payload")
    # @allure.testcase("", "TestRail")
    def testPlacingHold(self):
        actual = self.datalayer.get_payload_for_event('bc.placeHold')
        expected = {'gtmCategory': 'v2-holds', 'gtmAction': 'v2-place', 'gtmValue': 1}
        for key, value in expected.items():
            actual.should.have.key(key).being.equal(value)

    @allure.title("Analytics: Holds: cancelling a hold pushes event to dataLayer with expected payload")
    # @allure.testcase("", "TestRail")
    def testCancellingHold(self):
        actual = self.datalayer.get_payload_for_event('bc.cancelHold')
        expected = {'gtmCategory': 'v2-holds', 'gtmAction': 'v2-cancel', 'gtmValue': 1}
        for key, value in expected.items():
            actual.should.have.key(key).being.equal(value)
