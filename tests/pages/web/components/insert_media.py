from selenium.webdriver.common.by import By
from pypom import Region
from selenium.common.exceptions import ElementNotSelectableException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from .select_widget_image import SelectWidgetImage

class InsertMedia(SelectWidgetImage):

    _tabs_locator = (By.CSS_SELECTOR, "a.media-menu-item")

    @property
    def add_media(self):
        tabs = self.find_elements(*self._tabs_locator)
        return tabs[0]

    def select_image_from_list(self, image_position):
        self.lists = self.find_elements(*self._lists_of_images_locator)
        self.image = self.lists[0]
        images = self.image.find_elements(*self._images_in_list_locator)

        while(len(images) <= 1):
            self.lists = self.find_elements(*self._lists_of_images_locator)
            self.image = self.lists[0]
            images = self.image.find_elements(*self._images_in_list_locator)
            if(len(images) > image_position):
                return images[image_position]

        return images[image_position]

    @property
    def close_button(self):
        self.wait.until(EC.presence_of_all_elements_located(self._close_button_locator))
        close = self.find_elements(*self._close_button_locator)
        return close[0]

    @property
    def media_library_tab(self):
        tabs = self.find_elements(*self._tabs_locator)
        return tabs[5]

    class AddMedia(Region):

        _add_media_heading_locator = (By.CSS_SELECTOR, "#__wp-uploader-id-0 > div.media-frame-title > h1")
        _alignment_locator = (By.CSS_SELECTOR, "select.alignment")
        _link_to_locator = (By.CSS_SELECTOR, "select.link-to")
        _size_locator = (By.CSS_SELECTOR, "select.size")

        @property
        def loaded(self):
            return self.find_element(*self._add_media_heading_locator).text

        @property
        def alignment(self):
            return self.find_element(*self._alignment_locator)

        def select_alignment(self, value):
            options = ["Left", "Center", "Right", "None"]
            select = Select(self.alignment)
            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def link_to(self):
            return self.find_element(*self._link_to_locator)

        def select_link_to(self, value):
            options = ["None", "Media File", "Attachment Page", "Custom URL"]
            select = Select(self.link_to)
            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def size(self):
            return self.find_element(*self._size_locator)

        def select_size(self, value):
            options = ["Thumbnail – 150 × 150", "Medium – 300 × 150", "Large – 670 × 335", "Full Size – 1280 × 640"]
            select = Select(self.size)
            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def insert_into_post(self):
            return InsertMedia.add_image_to_widget_button

    @property
    def create_gallery(self):
        tabs = self.find_elements(*self._tabs_locator)
        return tabs[1]

    class CreateGallery(Region):

        _create_gallery_heading_locator = (By.CSS_SELECTOR, "#__wp-uploader-id-0 > div.media-frame-title > h1")

        @property
        def loaded(self):
            return self.find_element(*self._create_gallery_heading_locator).text

        @property
        def create_a_new_gallery(self):
            return InsertMedia.add_image_to_widget_button

    @property
    def featured_image(self):
        tabs = self.find_elements(*self._tabs_locator)
        return tabs[2]

    class FeaturedImage(Region):

        _featured_image_heading_locator = (By.CSS_SELECTOR, "#__wp-uploader-id-0 > div.media-frame-title > h1")

        @property
        def loaded(self):
            return self.find_element(*self._featured_image_heading_locator).text

        @property
        def set_featured_image(self):
            return InsertMedia.add_image_to_widget_button

    @property
    def insert_from_url(self):
        tabs = self.find_elements(*self._tabs_locator)
        return tabs[3]

    class InsertFromUrl(Region):

        _insert_from_url_heading_locator = (By.CSS_SELECTOR, "#__wp-uploader-id-0 > div.media-frame-title > h1")
        _url_locator = (By.CSS_SELECTOR, "input[id='embed-url-field']")
        _link_text_locator = (By.CSS_SELECTOR, "input.alignment")

        @property
        def loaded(self):
            return self.find_element(*self._insert_from_url_heading_locator).text

        @property
        def url(self):
            return self.find_element(*self._url_locator)

        @property
        def link_text(self):
            return self.find_element(*self._link_text_locator)

        @property
        def insert_into_post(self):
            return InsertMedia.add_image_to_widget_button
