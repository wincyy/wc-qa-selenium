from selenium.webdriver.common.by import By
from pypom import Region
from selenium.common.exceptions import NoSuchElementException
import re

class WPSideMenu(Region):

    _wp_admin_sidemenu_locator = (By.ID, "adminmenu")
    _collapse_menu_button_locator = (By.CSS_SELECTOR, "button[aria-expanded='true']")
    _expand_menu_button_locator = (By.CSS_SELECTOR, "button[aria-expanded='false']")
    _main_menu_list_locator = (By.CSS_SELECTOR, "li:not(.d-none) div.wp-menu-name")
    _submenu_list_locator = (By.CSS_SELECTOR, "li:not(.d-none) > ul.wp-submenu > li")
    _current_menu_item_locator = (By.CSS_SELECTOR, "li.wp-has-current-submenu div.wp-menu-name")
    _current_submenu_item_locator = (By.CSS_SELECTOR, "li:not(.d-none) ul > li.current")

    @property
    def wp_admin_sidemenu(self):
        return self.find_element(*self._wp_admin_sidemenu_locator)

    @property
    def is_wp_admin_sidemenu_displayed(self):
        try:
            return self.find_element(*self._wp_admin_sidemenu_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def collapse_menu(self):
        return self.find_element(*self._collapse_menu_button_locator)

    @property
    def expand_menu(self):
        return self.find_element(*self._expand_menu_button_locator)

    @property
    def is_menu_collapsed(self):
        try:
            return self.find_element(*self._expand_menu_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def main_menu_list(self):
        return self.wp_admin_sidemenu.find_elements(*self._main_menu_list_locator)

    @property
    def displayed_submenu_list(self):
        full_submenu_list = self.wp_admin_sidemenu.find_elements(*self._submenu_list_locator)
        return [e for e in full_submenu_list if e.text != ""]

    @property
    def current_menu_item(self):
        return self.find_element(*self._current_menu_item_locator)

    @property
    def current_submenu_item(self):
        return self.find_element(*self._current_submenu_item_locator)

    def transform_text(self, element):
        text = re.sub(r'[^a-zA-Z\d ]', ' ', element.text)
        text = re.sub(' +', ' ', text)
        return text.strip()

    def get_wp_admin_menu_option(self, option):
        for element in self.main_menu_list:
            if self.transform_text(element).casefold() == option.casefold():
                return element
                break
            else:
                pass
        return False

    def get_wp_admin_submenu_option(self, option):
        for element in self.displayed_submenu_list:
            if self.transform_text(element).casefold() == option.casefold():
                return element
                break
            else:
                pass
        return False

    # Menus
    @property
    def menu_dashboard(self):
        return self.get_wp_admin_menu_option("dashboard")

    @property
    def menu_all_content(self):
        return self.get_wp_admin_menu_option("all content")

    @property
    def menu_forms(self):
        return self.get_wp_admin_menu_option("forms")

    @property
    def menu_media_library(self):
        return self.get_wp_admin_menu_option("media library")

    @property
    def menu_card_taxonomies(self):
        return self.get_wp_admin_menu_option("card taxonomies")

    @property
    def menu_page_builder(self):
        return self.get_wp_admin_menu_option("page builder")

    @property
    def menu_settings(self):
        return self.get_wp_admin_menu_option("settings")

    @property
    def menu_tools(self):
        return self.get_wp_admin_menu_option("tools")

    @property
    def menu_users(self):
        return self.get_wp_admin_menu_option("users")

    @property
    def menu_edit_flow(self):
        return self.get_wp_admin_menu_option("edit flow")

    @property
    def menu_switch_to_v2_menu(self):
        return self.get_wp_admin_menu_option("switch to v2 menu")

    @property
    def menu_switch_to_v3_menu(self):
        return self.get_wp_admin_menu_option("switch to v3 menu")

    # Submenus
    @property
    def submenu_home(self):
        return self.get_wp_admin_submenu_option("home")

    @property
    def submenu_my_sites(self):
        return self.get_wp_admin_submenu_option("my sites")

    @property
    def submenu_user_searches(self):
        return self.get_wp_admin_submenu_option("user searches")

    @property
    def submenu_admin_search(self):
        return self.get_wp_admin_submenu_option("admin search")

    @property
    def submenu_all_cards(self):
        return self.get_wp_admin_submenu_option("all cards")

    @property
    def submenu_all_pages(self):
        return self.get_wp_admin_submenu_option("all pages")

    @property
    def submenu_all_blog_posts(self):
        return self.get_wp_admin_submenu_option("all blog posts")

    @property
    def submenu_all_news(self):
        return self.get_wp_admin_submenu_option("all news")

    @property
    def submenu_all_faqs(self):
        return self.get_wp_admin_submenu_option("all faqs")

    @property
    def submenu_all_online_resources(self):
        return self.get_wp_admin_submenu_option("all online resources")

    @property
    def submenu_all_special_content(self):
        return self.get_wp_admin_submenu_option("all special content")

    @property
    def submenu_all_archival_collections(self):
        return self.get_wp_admin_submenu_option("all archival collections")

    @property
    def submenu_all_hero_slides(self):
        return self.get_wp_admin_submenu_option("all hero slides")

    @property
    def submenu_all_banners(self):
        return self.get_wp_admin_submenu_option("all banners")

    @property
    def submenu_forms(self):
        return self.get_wp_admin_submenu_option("forms")

    @property
    def submenu_new_form(self):
        return self.get_wp_admin_submenu_option("new form")

    @property
    def submenu_form_entries(self):
        return self.get_wp_admin_submenu_option("form entries")

    @property
    def submenu_export_forms(self):
        return self.get_wp_admin_submenu_option("export forms")

    @property
    def submenu_site_settings(self):
        return self.get_wp_admin_submenu_option("site settings")

    @property
    def submenu_add_ons(self):
        return self.get_wp_admin_submenu_option("add ons")

    @property
    def submenu_system_status(self):
        return self.get_wp_admin_submenu_option("system status")

    @property
    def submenu_media_library(self):
        return self.get_wp_admin_submenu_option("media library")

    @property
    def submenu_add_new_media(self):
        return self.get_wp_admin_submenu_option("add new media")

    @property
    def submenu_bulk_optimize(self):
        return self.get_wp_admin_submenu_option("bulk optimize")

    @property
    def submenu_audiences(self):
        return self.get_wp_admin_submenu_option("audiences")

    @property
    def submenu_related_formats(self):
        return self.get_wp_admin_submenu_option("related formats")

    @property
    def submenu_programs_campaigns(self):
        return self.get_wp_admin_submenu_option("programs campaigns")

    @property
    def submenu_genres(self):
        return self.get_wp_admin_submenu_option("genres")

    @property
    def submenu_topics(self):
        return self.get_wp_admin_submenu_option("topics")

    @property
    def submenu_tags(self):
        return self.get_wp_admin_submenu_option("tags")

    @property
    def submenu_location(self):
        return self.get_wp_admin_submenu_option("location")

    @property
    def submenu_templates(self):
        return self.get_wp_admin_submenu_option("templates")

    @property
    def submenu_saved_rows(self):
        return self.get_wp_admin_submenu_option("saved rows")

    @property
    def submenu_saved_columns(self):
        return self.get_wp_admin_submenu_option("saved columns")

    @property
    def submenu_saved_modules(self):
        return self.get_wp_admin_submenu_option("saved modules")

    @property
    def submenu_categories(self):
        return self.get_wp_admin_submenu_option("categories")

    @property
    def submenu_add_new(self):
        return self.get_wp_admin_submenu_option("add new")

    @property
    def submenu_default_settings(self):
        return self.get_wp_admin_submenu_option("default settings")

    @property
    def submenu_menus(self):
        return self.get_wp_admin_submenu_option("menus")

    @property
    def submenu_widgets(self):
        return self.get_wp_admin_submenu_option("widgets")

    @property
    def submenu_bibliocommons_site_settings(self):
        return self.get_wp_admin_submenu_option("bibliocommons site settings")

    @property
    def submenu_general_settings(self):
        return self.get_wp_admin_submenu_option("general settings")

    @property
    def submenu_seo_descriptions(self):
        return self.get_wp_admin_submenu_option("seo descriptions")

    @property
    def submenu_library_locations_settings(self):
        return self.get_wp_admin_submenu_option("library locations settings")

    @property
    def submenu_upload_branches_xml(self):
        return self.get_wp_admin_submenu_option("upload branches xml")

    @property
    def submenu_writing(self):
        return self.get_wp_admin_submenu_option("writing")

    @property
    def submenu_reading(self):
        return self.get_wp_admin_submenu_option("reading")

    @property
    def submenu_page_builder(self):
        return self.get_wp_admin_submenu_option("page builder")

    @property
    def submenu_third_party_scripts(self):
        return self.get_wp_admin_submenu_option("third party scripts")

    @property
    def submenu_post_expirator(self):
        return self.get_wp_admin_submenu_option("post expirator")

    @property
    def submenu_alpinetile_flickr(self):
        return self.get_wp_admin_submenu_option("alpinetile flickr")

    @property
    def submenu_permalinks(self):
        return self.get_wp_admin_submenu_option("permalinks")

    @property
    def submenu_relevanssi_premium(self):
        return self.get_wp_admin_submenu_option("relevanssi premium")

    @property
    def submenu_link_checker(self):
        return self.get_wp_admin_submenu_option("link checker")

    @property
    def submenu_v2_taxonomy_migrations_all(self):
        return self.get_wp_admin_submenu_option("v2 taxonomy migrations all")

    @property
    def submenu_v2_taxonomy_migrations_no_tags(self):
        return self.get_wp_admin_submenu_option("v2 taxonomy migrations no tags")

    @property
    def submenu_ewww_image_optimizer(self):
        return self.get_wp_admin_submenu_option("ewww image optimizer")

    @property
    def submenu_cache_browser(self):
        return self.get_wp_admin_submenu_option("cache browser")

    @property
    def submenu_html_widget(self):
        return self.get_wp_admin_submenu_option("html widget")

    @property
    def submenu_plugins(self):
        return self.get_wp_admin_submenu_option("plugins")

    @property
    def submenu_broken_links(self):
        return self.get_wp_admin_submenu_option("broken links")

    @property
    def submenu_redirection(self):
        return self.get_wp_admin_submenu_option("redirection")

    @property
    def submenu_export(self):
        return self.get_wp_admin_submenu_option("export")

    @property
    def submenu_export_site_content(self):
        return self.get_wp_admin_submenu_option("export site content")

    @property
    def submenu_import(self):
        return self.get_wp_admin_submenu_option("import")

    @property
    def submenu_domain_mapping(self):
        return self.get_wp_admin_submenu_option("domain mapping")

    @property
    def submenu_export_personal_data(self):
        return self.get_wp_admin_submenu_option("export personal data")

    @property
    def submenu_erase_personal_data(self):
        return self.get_wp_admin_submenu_option("erase personal data")

    @property
    def submenu_biblio_api_validation(self):
        return self.get_wp_admin_submenu_option("biblio api validation")

    @property
    def submenu_your_profile(self):
        return self.get_wp_admin_submenu_option("your profile")

    @property
    def submenu_add_new_users(self):
        return self.get_wp_admin_submenu_option("add new users")

    @property
    def submenu_all_users(self):
        return self.get_wp_admin_submenu_option("all users")

    @property
    def submenu_edit_flow(self):
        return self.get_wp_admin_submenu_option("edit flow")

    @property
    def submenu_dashboard_widgets(self):
        return self.get_wp_admin_submenu_option("dashboard widgets")

    @property
    def submenu_editorial_comments(self):
        return self.get_wp_admin_submenu_option("editorial comments")

    @property
    def submenu_notifications(self):
        return self.get_wp_admin_submenu_option("notifications")

    # V2 only - menu
    @property
    def v2_menu_blog_posts(self):
        return self.get_wp_admin_menu_option("blog posts")

    @property
    def v2_menu_media(self):
        return self.get_wp_admin_menu_option("media")

    @property
    def v2_menu_pages(self):
        return self.get_wp_admin_menu_option("pages")

    @property
    def v2_menu_comments(self):
        return self.get_wp_admin_menu_option("comments")

    @property
    def v2_menu_carousels(self):
        return self.get_wp_admin_menu_option("carousels")

    @property
    def v2_menu_content_cards(self):
        return self.get_wp_admin_menu_option("content cards")

    @property
    def v2_menu_special_content(self):
        return self.get_wp_admin_menu_option("special content")

    @property
    def v2_menu_faqs(self):
        return self.get_wp_admin_menu_option("faqs")

    @property
    def v2_menu_news(self):
        return self.get_wp_admin_menu_option("news")

    @property
    def v2_menu_online_resources(self):
        return self.get_wp_admin_menu_option("online resources")

    @property
    def v2_menu_archives_collections(self):
        return self.get_wp_admin_menu_option("archives collections")

    @property
    def v2_menu_appearance(self):
        return self.get_wp_admin_menu_option("appearance")

    @property
    def v2_menu_plugins(self):
        return self.get_wp_admin_menu_option("plugins")

    @property
    def v2_menu_switch_to_v3_menu(self):
        return self.get_wp_admin_menu_option("switch to v3 menu")

    @property
    def v2_menu_instagram_feed(self):
        return self.get_wp_admin_menu_option("instagram feed")

    # V2 only - submenu
    @property
    def v2_submenu_blog_categories(self):
        return self.get_wp_admin_submenu_option("blog categories")

    @property
    def v2_submenu_locations(self):
        return self.get_wp_admin_submenu_option("locations")

    @property
    def v2_submenu_library(self):
        return self.get_wp_admin_submenu_option("library")

    @property
    def v2_submenu_entries(self):
        return self.get_wp_admin_submenu_option("entries")

    @property
    def v2_submenu_settings(self):
        return self.get_wp_admin_submenu_option("settings")

    @property
    def v2_submenu_import_export(self):
        return self.get_wp_admin_submenu_option("import export")

    @property
    def v2_submenu_all_carousels(self):
        return self.get_wp_admin_submenu_option("all carousels")

    @property
    def v2_submenu_add_a_new_carousel(self):
        return self.get_wp_admin_submenu_option("add a new carousel")

    @property
    def v2_submenu_browse_pages(self):
        return self.get_wp_admin_submenu_option("browse pages")

    @property
    def v2_submenu_carousel_categories(self):
        return self.get_wp_admin_submenu_option("carousel categories")

    @property
    def v2_submenu_order_browse_buttons(self):
        return self.get_wp_admin_submenu_option("order browse buttons")

    @property
    def v2_submenu_manage_browse_pages(self):
        return self.get_wp_admin_submenu_option("manage browse pages")

    @property
    def v2_submenu_all_content_cards(self):
        return self.get_wp_admin_submenu_option("all content cards")

    @property
    def v2_submenu_add_a_new_card(self):
        return self.get_wp_admin_submenu_option("add a new card")

    @property
    def v2_submenu_channel_labels(self):
        return self.get_wp_admin_submenu_option("channel labels")

    @property
    def v2_submenu_archive_categories(self):
        return self.get_wp_admin_submenu_option("archive categories")

    @property
    def v2_submenu_manage_homepage(self):
        return self.get_wp_admin_submenu_option("manage homepage")

    @property
    def v2_submenu_hero_block(self):
        return self.get_wp_admin_submenu_option("hero block")

    @property
    def v2_submenu_all_content(self):
        return self.get_wp_admin_submenu_option("all content")

    @property
    def v2_submenu_add_new_content(self):
        return self.get_wp_admin_submenu_option("add new content")

    @property
    def v2_submenu_special_content_categories(self):
        return self.get_wp_admin_submenu_option("special content categories")

    @property
    def v2_submenu_special_content_settings(self):
        return self.get_wp_admin_submenu_option("special content settings")

    @property
    def v2_submenu_add_new_faq(self):
        return self.get_wp_admin_submenu_option("add new faq")

    @property
    def v2_submenu_faq_categories(self):
        return self.get_wp_admin_submenu_option("faq categories")

    @property
    def v2_submenu_faq_category_order(self):
        return self.get_wp_admin_submenu_option("faq category order")

    @property
    def v2_submenu_faq_settings(self):
        return self.get_wp_admin_submenu_option("faq settings")

    @property
    def v2_submenu_add_new_article(self):
        return self.get_wp_admin_submenu_option("add new article")

    @property
    def v2_submenu_news_category(self):
        return self.get_wp_admin_submenu_option("news category")

    @property
    def v2_submenu_news_settings(self):
        return self.get_wp_admin_submenu_option("news settings")

    @property
    def v2_submenu_add_new_resource(self):
        return self.get_wp_admin_submenu_option("add new resource")

    @property
    def v2_submenu_online_resource_subject(self):
        return self.get_wp_admin_submenu_option("online resource subject")

    @property
    def v2_submenu_learning_tools(self):
        return self.get_wp_admin_submenu_option("learning tools")

    @property
    def v2_submenu_kids_subject(self):
        return self.get_wp_admin_submenu_option("kids subject")

    @property
    def v2_submenu_teen_subject(self):
        return self.get_wp_admin_submenu_option("teen subject")

    @property
    def v2_submenu_format(self):
        return self.get_wp_admin_submenu_option("format")

    @property
    def v2_submenu_online_resource_settings(self):
        return self.get_wp_admin_submenu_option("online resource settings")

    @property
    def v2_submenu_all_collections(self):
        return self.get_wp_admin_submenu_option("all collections")

    @property
    def v2_submenu_add_new_collection(self):
        return self.get_wp_admin_submenu_option("add new collection")

    @property
    def v2_submenu_archival_collections_subjects(self):
        return self.get_wp_admin_submenu_option("archival collections subjects")

    @property
    def v2_submenu_archival_collection_settings(self):
        return self.get_wp_admin_submenu_option("archival collection settings")

    @property
    def v2_submenu_themes(self):
        return self.get_wp_admin_submenu_option("themes")

    @property
    def v2_submenu_available_tools(self):
        return self.get_wp_admin_submenu_option("available tools")

    @property
    def v2_submenu_general(self):
        return self.get_wp_admin_submenu_option("general")

    @property
    def v2_submenu_media(self):
        return self.get_wp_admin_submenu_option("media")

    @property
    def v2_submenu_creative_commons_taggers(self):
        return self.get_wp_admin_submenu_option("creative commons taggers")

    @property
    def v2_submenu_languages(self):
        return self.get_wp_admin_submenu_option("languages")

    @property
    def v2_submenu_bibliocommons_settings(self):
        return self.get_wp_admin_submenu_option("bibliocommons settings")

    @property
    def v2_submenu_homepage_banner(self):
        return self.get_wp_admin_submenu_option("homepage banner")

    @property
    def v2_submenu_homepage_new_titles(self):
        return self.get_wp_admin_submenu_option("homepage new titles")

    @property
    def v2_submenu_view_share_widget(self):
        return self.get_wp_admin_submenu_option("view share widget")

    @property
    def v2_submenu_program(self):
        return self.get_wp_admin_submenu_option("program")

    @property
    def v2_submenu_program_block(self):
        return self.get_wp_admin_submenu_option("program block")

    @property
    def v2_submenu_program_banners(self):
        return self.get_wp_admin_submenu_option("program banners")