from selenium.webdriver.common.by import By
from pages.web.components.page_builder.card_base import CardBase
from selenium.common.exceptions import ElementNotSelectableException
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from pypom import Region

class SingleCard(CardBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='SingleCard']")

    @property
    def loaded(self):
        return self.find_element(*self._heading_locator)
