from selenium.webdriver.common.by import By
from pypom import Region
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.color import Color

class WPHeader(Region):

    _wp_admin_header_locator = (By.ID, "wpadminbar")

    _my_sites_locator = (By.ID, "wp-admin-bar-my-sites")
    _my_sites_submenu_locator = (By.CSS_SELECTOR, "[id='wp-admin-bar-my-sites'] > div")
    _my_sites_network_admin_locator = (By.ID, "wp-admin-bar-network-admin']")
    _my_sites_network_admin_submenu_locator = (By.CSS_SELECTOR, "[id='wp-admin-bar-network-admin'] > div")
    _my_sites_network_admin_dashboard_locator = (By.ID, "wp-admin-bar-network-admin-d")
    _my_sites_network_admin_sites_locator = (By.ID, "wp-admin-bar-network-admin-s")
    _my_sites_network_admin_users_locator = (By.ID, "wp-admin-bar-network-admin-u")
    _my_sites_network_admin_themes_locator = (By.ID, "wp-admin-bar-network-admin-t")
    _my_sites_network_admin_plugins_locator = (By.ID, "wp-admin-bar-network-admin-p")
    _my_sites_network_admin_settings_locator = (By.ID, "wp-admin-bar-network-admin-o")
    _my_sites_list_locator = (By.ID, "wp-admin-bar-my-sites-list")
    _my_sites_first_locator = (By.CSS_SELECTOR, "[id='wp-admin-bar-my-sites-list'] > li:first-child")
    _my_sites_first_submenu_locator = (By.CSS_SELECTOR, "[id='wp-admin-bar-my-sites-list'] > li:first-child > div")

    # Assumption: Only one site in the list
    _my_sites_submenu_dashboard_locator = (By.CSS_SELECTOR, "[id^='wp-admin-bar-blog-'][id$='-d']")
    _my_sites_submenu_new_post_locator = (By.CSS_SELECTOR, "[id^='wp-admin-bar-blog-'][id$='-n']")
    _my_sites_submenu_manage_comments_locator = (By.CSS_SELECTOR, "[id^='wp-admin-bar-blog-'][id$='-c']")
    _my_sites_submenu_visit_site_locator = (By.CSS_SELECTOR, "[id^='wp-admin-bar-blog-'][id$='-v']")

    # My Site Name
    _my_site_name_locator = (By.ID, "wp-admin-bar-site-name")
    _my_site_name_submenu_locator = (By.CSS_SELECTOR, "[id='wp-admin-bar-site-name'] > div")
    _my_site_visit_locator = (By.ID, "wp-admin-bar-view-site")
    _my_site_edit_locator = (By.ID, "wp-admin-bar-edit-site")
    _my_site_dashboard_locator = (By.ID, "wp-admin-bar-dashboard")
    _my_site_themes_locator = (By.ID, "wp-admin-bar-themes")
    _my_site_widgets_locator = (By.ID, "wp-admin-bar-widgets")
    _my_site_menus_locator = (By.ID, "wp-admin-bar-menus")

    # My Comments
    _my_unread_comments_locator = (By.ID, "wp-admin-bar-comments")
    _my_unread_comments_count_locator = (By.CSS_SELECTOR, "[id='wp-admin-bar-comments'] > a > span[class*='ab-label']")

    # Add New Content
    _add_new_content_locator = (By.ID, "wp-admin-bar-new-content")
    _add_new_content_label_locator = (By.CSS_SELECTOR, "[id='wp-admin-bar-new-content'] > a > span[class*='ab-label']")
    _add_new_content_submenu_locator = (By.CSS_SELECTOR, "[id='wp-admin-bar-new-content'] > div")
    _add_new_content_submenu_list_locator = (By.CSS_SELECTOR, "[id='wp-admin-bar-new-content-default'] > li")

    # Add New Content - v3 only
    _new_card_locator = (By.ID, "wp-admin-bar-new-bw_content_card")
    _new_custom_pg_locator = (By.ID, "wp-admin-bar-new-custom_page")
    _new_standard_pg_locator = (By.ID, "wp-admin-bar-new-standard_page")

    # Add New Content - v2 and v3 shared
    _new_blog_post_locator = (By.ID, "wp-admin-bar-new-post")
    _new_media_locator = (By.ID, "wp-admin-bar-new-media")
    _new_faq_locator = (By.ID, "wp-admin-bar-new-bccms_faq")
    _new_news_post_locator = (By.ID, "wp-admin-bar-new-bccms_news")
    _new_online_resource_locator = (By.ID, "wp-admin-bar-new-bccms_online_link")
    _new_user_locator = (By.ID, "wp-admin-bar-new-user")
    _new_form_locator = (By.ID, "wp-admin-bar-gravityforms-new-form")

    # Add New Content - v2 only
    _new_v2_pg_locator = (By.ID, "wp-admin-bar-new-page")
    _new_special_content_locator = (By.ID, "wp-admin-bar-new-bc_custom_content")
    _new_archive_collection_locator = (By.ID, "wp-admin-bar-new-bccms_archival_post")
    _new_v2_carousel_locator = (By.ID, "wp-admin-bar-new-bc_browse_carousel")
    _new_v2_card_locator = (By.ID, "wp-admin-bar-new-bc_content_card")

    # Edit/View Content
    _edit_locator = (By.ID, "wp-admin-bar-edit")
    _view_locator = (By.ID, "wp-admin-bar-view")

    # Page Builder
    _page_builder_locator = (By.ID, "wp-admin-bar-fl-builder-frontend-edit-link")
    _page_builder_dot_locator = (By.CSS_SELECTOR, "[id='wp-admin-bar-fl-builder-frontend-edit-link'] > a > "
                                                  "[class='fl-builder-admin-bar-status-dot']")
    # My Account
    _my_account_locator = (By.ID, "wp-admin-bar-my-account")
    _my_account_display_name_locator = (By.CSS_SELECTOR, "[id='wp-admin-bar-my-account'] > a > span")
    _my_account_actions_locator = (By.ID, "wp-admin-bar-user-actions")
    _my_account_user_info_locator = (By.ID, "wp-admin-bar-user-info")
    _my_account_edit_profile_locator = (By.ID, "wp-admin-bar-edit-profile")
    _my_account_log_out_locator = (By.ID, "wp-admin-bar-logout")

    # Least used / network admin only
    _customize_locator = (By.ID, "wp-admin-bar-customize")
    _updates_locator = (By.ID, "wp-admin-bar-updates")
    _updates_count_locator = (By.CSS_SELECTOR, "[id='wp-admin-bar-updates'] > a > span[class*='ab-label']")

    @property
    def wp_admin_header(self):
        return self.find_element(*self._wp_admin_header_locator)

    @property
    def is_wp_admin_header_displayed(self):
        try:
            return self.find_element(*self._wp_admin_header_locator).is_displayed()
        except NoSuchElementException:
            return False

    # My Sites
    @property
    def my_sites(self):
        return self.find_element(*self._my_sites_locator)

    @property
    def is_my_sites_menu_item_displayed(self):
        try:
            return self.find_element(*self._my_sites_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_my_sites_submenu_displayed(self):
        try:
            return self.find_element(*self._my_sites_submenu_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_sites_network_admin(self):
        return self.find_element(*self._my_sites_network_admin_locator)

    @property
    def is_my_sites_network_admin_locator_displayed(self):
        try:
            return self.find_element(*self._my_sites_network_admin_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_sites_list(self):
        return self.find_elements(*self._my_sites_list_locator)

    @property
    def is_my_sites_list_displayed(self):
        try:
            return self.find_element(*self._my_sites_list_locator).is_displayed()
        except NoSuchElementException:
            return False

    # My Sites - Network Admin - submenu
    @property
    def is_my_sites_network_admin_submenu_displayed(self):
        try:
            return self.find_element(*self._my_sites_network_admin_submenu_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_sites_network_admin_dashboard(self):
        return self.find_element(*self._my_sites_network_admin_dashboard_locator)

    @property
    def my_sites_network_admin_sites(self):
        return self.find_element(*self._my_sites_network_admin_sites_locator)

    @property
    def my_sites_network_admin_users(self):
        return self.find_element(*self._my_sites_network_admin_users_locator)

    @property
    def my_sites_network_admin_themes(self):
        return self.find_element(*self._my_sites_network_admin_themes_locator)

    @property
    def my_sites_network_admin_plugins(self):
        return self.find_element(*self._my_sites_network_admin_plugins_locator)

    @property
    def my_sites_network_admin_settings(self):
        return self.find_element(*self._my_sites_network_admin_settings_locator)

    # My Sites - the only site
    @property
    def my_sites_first(self):
        return self.find_element(*self._my_sites_first_locator)

    @property
    def is_my_sites_first_submenu_displayed(self):
        try:
            return self.find_element(*self._my_sites_first_submenu_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_sites_submenu_dashboard(self):
        return self.find_element(*self._my_sites_submenu_dashboard_locator)

    @property
    def is_my_sites_submenu_dashboard_displayed(self):
        try:
            return self.my_sites_submenu_dashboard.is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_sites_submenu_new_post(self):
        return self.find_element(*self._my_sites_submenu_new_post_locator)

    @property
    def is_my_sites_submenu_new_post_displayed(self):
        try:
            return self.my_sites_submenu_new_post.is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_sites_submenu_manage_comments(self):
        return self.find_element(*self._my_sites_submenu_manage_comments_locator)

    @property
    def is_my_sites_submenu_manage_comments_displayed(self):
        try:
            return self.my_sites_submenu_manage_comments.is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_sites_submenu_visit_site(self):
        return self.find_element(*self._my_sites_submenu_visit_site_locator)

    @property
    def is_my_sites_submenu_list_visit_site_displayed(self):
        try:
            return self.my_sites_submenu_visit_site.is_displayed()
        except NoSuchElementException:
            return False

    # My Site Name
    @property
    def my_site_name(self):
        return self.find_element(*self._my_site_name_locator)

    @property
    def is_my_site_name_menu_item_displayed(self):
        try:
            return self.find_element(*self._my_site_name_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_my_site_name_submenu_displayed(self):
        try:
            return self.find_element(*self._my_site_name_submenu_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_site_visit(self):
        return self.find_element(*self._my_site_visit_locator)

    @property
    def is_my_site_visit_displayed(self):
        try:
            return self.find_element(*self._my_site_visit_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_site_edit(self):
        return self.find_element(*self._my_site_edit_locator)

    @property
    def is_my_site_edit_displayed(self):
        try:
            return self.find_element(*self._my_site_edit_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_site_dashboard(self):
        return self.find_element(*self._my_site_dashboard_locator)

    @property
    def is_my_site_dashboard_displayed(self):
        try:
            return self.find_element(*self._my_site_dashboard_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_site_themes(self):
        return self.find_element(*self._my_site_themes_locator)

    @property
    def is_my_site_themes_displayed(self):
        try:
            return self.find_element(*self._my_site_themes_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_site_widgets(self):
        return self.find_element(*self._my_site_widgets_locator)

    @property
    def is_my_site_widgets_displayed(self):
        try:
            return self.find_element(*self._my_site_widgets_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_site_menus(self):
        return self.find_element(*self._my_site_widgets_locator)

    @property
    def is_my_site_menus_displayed(self):
        try:
            return self.find_element(*self._my_site_widgets_locator).is_displayed()
        except NoSuchElementException:
            return False

    # Customize
    @property
    def customize(self):
        return self.find_element(*self._customize_locator)

    @property
    def is_customize_menu_item_displayed(self):
        try:
            return self.find_element(*self._customize_locator).is_displayed()
        except NoSuchElementException:
            return False

    # Updates
    @property
    def updates(self):
        return self.find_element(*self._updates_locator)

    @property
    def is_updates_menu_item_displayed(self):
        try:
            return self.find_element(*self._updates_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_updates_count(self):
        return self.find_element(*self._updates_count_locator)

    # Unread comments
    @property
    def my_unread_comments(self):
        return self.find_element(*self._my_unread_comments_locator)

    @property
    def is_my_unread_comments_menu_item_displayed(self):
        try:
            return self.find_element(*self._my_unread_comments_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_unread_comments_count(self):
        return self.find_element(*self._my_unread_comments_count_locator)

    # Add New Content
    @property
    def add_new_content(self):
        return self.find_element(*self._add_new_content_locator)

    @property
    def is_add_new_content_menu_item_displayed(self):
        try:
            return self.find_element(*self._add_new_content_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def add_new_content_label(self):
        return self.find_element(*self._add_new_content_label_locator)

    @property
    def is_add_new_content_submenu_displayed(self):
        try:
            return self.find_element(*self._add_new_content_submenu_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def add_new_content_submenu_list(self):
        return self.find_elements(*self._add_new_content_submenu_list_locator)

    # Add New Content submenu - v3 only
    @property
    def add_new_card(self):
        return self.find_element(*self._new_card_locator)

    @property
    def add_new_custom_pg(self):
        return self.find_element(*self._new_custom_pg_locator)

    @property
    def add_new_standard_pg(self):
        return self.find_element(*self._new_standard_pg_locator)

    # Add New Content submenu - v2 and v3 shared
    @property
    def add_new_blog_post(self):
        return self.find_element(*self._new_blog_post_locator)

    @property
    def add_new_news_post(self):
        return self.find_element(*self._new_news_post_locator)

    @property
    def add_new_faq(self):
        return self.find_element(*self._new_faq_locator)

    @property
    def add_new_online_resource(self):
        return self.find_element(*self._new_online_resource_locator)

    @property
    def add_new_form(self):
        return self.find_element(*self._new_form_locator)

    @property
    def add_new_media(self):
        return self.find_element(*self._new_media_locator)

    @property
    def add_new_user(self):
        return self.find_element(*self._new_user_locator)

    # Add New Content submenu - v2 only
    @property
    def add_new_v2_pg(self):
        return self.find_element(*self._new_v2_pg_locator)

    @property
    def add_new_special_content(self):
        return self.find_element(*self._new_special_content_locator)

    @property
    def add_new_archive_collection(self):
        return self.find_element(*self._new_archive_collection_locator)

    @property
    def add_new_v2_carousel(self):
        return self.find_element(*self._new_v2_carousel_locator)

    @property
    def add_new_v2_card(self):
        return self.find_element(*self._new_v2_card_locator)

    # Edit content
    @property
    def edit_content(self):
        return self.find_element(*self._edit_locator)

    @property
    def is_edit_content_menu_item_displayed(self):
        try:
            return self.find_element(*self._edit_locator).is_displayed()
        except NoSuchElementException:
            return False

    # View content
    @property
    def view_content(self):
        return self.find_element(*self._view_locator)

    @property
    def is_view_content_menu_item_displayed(self):
        try:
            return self.find_element(*self._view_locator).is_displayed()
        except NoSuchElementException:
            return False

    # Page builder
    @property
    def page_builder(self):
        return self.find_element(*self._page_builder_locator)

    @property
    def is_page_builder_menu_item_displayed(self):
        try:
            return self.find_element(*self._page_builder_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def page_builder_dot_colour(self):
        element = self.find_element(*self._page_builder_dot_locator).value_of_css_property("color")
        return Color.from_string(element).hex

    # My Account
    @property
    def my_account(self):
        return self.find_element(*self._my_account_locator)

    @property
    def is_my_account_menu_item_displayed(self):
        try:
            return self.find_element(*self._my_account_locator).is_displayed()
        except NoSuchElementException:
            return False

    # My Account - submenu
    @property
    def is_my_account_submenu_displayed(self):
        try:
            return self.find_element(*self._my_account_actions_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def my_account_display_name(self):
        return self.find_element(*self._my_account_display_name_locator)

    @property
    def my_account_user_info(self):
        return self.find_element(*self._my_account_user_info_locator)

    @property
    def my_account_edit_profile(self):
        return self.find_element(*self._my_account_edit_profile_locator)

    @property
    def my_account_log_out(self):
        return self.find_element(*self._my_account_log_out_locator)

    def move_to_element(self, element):
        ActionChains(self.driver).move_to_element(element).perform()
        return element
