from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementNotSelectableException, NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select

from pages.web.wpadmin.v3.create_new_featured_block.new_banner_and_hero_slide_base import BannerHeroSlideBasePage

class CreateNewHeroSlidePage(BannerHeroSlideBasePage):

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _name_of_slide_locator = (By.CSS_SELECTOR, "input#fm-bw_hero-0-bw_hero_name-0")
    _slide_link_url_locator = (By.CSS_SELECTOR, "input#fm-bw_hero-0-bw_hero_url-0")
    _desktop_image_locator = (By.CSS_SELECTOR, "button#js--open-pick--desktop")
    _tablet_large_mobile_image_locator = (By.CSS_SELECTOR, "button#js--open-pick--mobile_large")
    _small_mobile_image_locator = (By.CSS_SELECTOR, "button#js--open-pick--mobile_small")

    #Taxonomies
    _audience_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_audience_category-0']")
    _related_format_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_related_format-0']")
    _programs_and_campaigns_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bc_program-0']")
    _genre_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_tax_genre-0']")
    _topic_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_tax_topic-0']")
    _tags_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-post_tag-0']")

    #Free text tag
    _free_text_tags_input_locator = (By.CSS_SELECTOR, "input#new-tag-post_tag")

    @property
    def page_heading(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def name_of_slide(self):
        return self.find_element(*self._name_of_slide_locator)

    @property
    def slide_link_url(self):
        return self.find_element(*self._slide_link_url_locator)

    @property
    def desktop_image(self):
        return self.find_element(*self._desktop_image_locator)

    @property
    def tablet_large_mobile_image(self):
        return self.find_element(*self._tablet_large_mobile_image_locator)

    @property
    def small_mobile_image(self):
        return self.find_element(*self._small_mobile_image_locator)

    def is_taxonomy_displayed(self, taxonomy):
        taxonomies = {
            "audience": self._audience_dropdown_locator,
            "related format": self._related_format_dropdown_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_locator,
            "genre": self._genre_dropdown_locator,
            "topic": self._topic_dropdown_locator,
            "tags": self._tags_dropdown_locator
        }

        try:
            return self.find_element(*(taxonomies.get(taxonomy.casefold()))).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_free_text_tags_field_displayed(self):
        try:
            return self.find_element(*self._free_text_tags_input_locator).is_displayed()
        except NoSuchElementException:
            return False
