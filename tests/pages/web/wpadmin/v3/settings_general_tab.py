from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from pages.web.components.select_widget_image import SelectWidgetImage

from .settings_base import SettingsBasePage

class SettingsGeneralTabPage(SettingsBasePage):

    #Library Contact Information Locators
    _library_main_phone_line_locator = (By.CSS_SELECTOR, "input[id='bc_lib_phone']")
    _contact_us_url_locator = (By.CSS_SELECTOR, "input[id='bc_contact_us_url']")
    _feedback_button_text_locator =(By.CSS_SELECTOR, "input[id='bc_feedback_text']")
    _feedback_button_url_locator = (By.CSS_SELECTOR, "input[id='bc_feedback_url']")

    #Library Policy Information Locators
    _url_to_privacy_page_locator = (By.CSS_SELECTOR, "input[id='bc_privacy_page_link']")
    _url_to_term_of_use_page_locator = (By.CSS_SELECTOR, "input[id='bc_terms_page_link']")

    #Get A Library Card Options Locators
    _get_a_library_card_link_url_locator = (By.CSS_SELECTOR, "input[id='bccms_get_a_card_href']")

    #Card Label And Color Configurations Locators
    _staff_catalog_title_label_locator = (By.CSS_SELECTOR, "input[id='bc_staff_catalog_title_label']")
    _staff_list_label_locator = (By.CSS_SELECTOR, "input[id='bc_staff_list_label']")
    _custom_label_for_other_resource_locator = (By.CSS_SELECTOR, "input[id='bc_staff_custom_label_other_resources']")

    #Footer Links Locators
    _city_link_locator = (By.CSS_SELECTOR, "input[id='bc_city_link']")
    _city_name_locator = (By.CSS_SELECTOR, "input[id='bc_city_name']")
    _other_footer_link_locator = (By.CSS_SELECTOR, "input[id='bc_other_footer_link']")
    _other_footer_link_text_locator = (By.CSS_SELECTOR, "input[id='bc_other_footer_link_name']")
    _url_for_support_link_locator = (By.CSS_SELECTOR, "input[id='bccms_support_lib_link']")

    #Social Media Links In Footer Locators
    _facebook_locator = (By.CSS_SELECTOR, "input[id='bccms_facebook_link']")
    _twitter_locator = (By.CSS_SELECTOR, "input[id='bccms_twitter_link']")
    _flickr_locator = (By.CSS_SELECTOR, "input[id='bccms_flickr_link']")
    _instagram_locator = (By.CSS_SELECTOR, "input[id='bccms_instagram_link']")
    _youtube_locator = (By.CSS_SELECTOR, "input[id='bccms_youtube_link']")
    _pinterest_locator = (By.CSS_SELECTOR, "input[id='bccms_pinterest_link']")
    _tumblr_locator = (By.CSS_SELECTOR, "input[id='bccms_tumblr_link']")
    _newsletter_locator = (By.CSS_SELECTOR, "input[id='bccms_newsletter_link']")

    #V3 Locators
    _enable_locator = (By.CSS_SELECTOR, "input[value='1']")
    _disable_locator = (By.CSS_SELECTOR, "input[value='0']")

    #Image Locator
    _image_locator = (By.CSS_SELECTOR, "tr > td > div > a.bccms-media-trigger.button")

    @property
    def loaded(self):
        return self.TabNavigation.general

    @property
    def library_main_phone_line(self):
        return self.find_element(*self._library_main_phone_line_locator)

    @property
    def contact_us_url(self):
        return self.find_element(*self._contact_us_url_locator)

    @property
    def feedback_button_text(self):
        return self.find_element(*self._feedback_button_text_locator)

    @property
    def feedback_button_url(self):
        return self.find_element(*self._feedback_button_url_locator)

    @property
    def url_to_privacy_page(self):
        return self.find_element(*self._url_to_privacy_page_locator)

    @property
    def url_to_terms_of_use_page(self):
        return self.find_element(*self._url_to_term_of_use_page_locator)

    @property
    def get_a_library_card_image(self):
        elements = self.find_elements(*self._image_locator)
        return elements[0]

    @property
    def get_a_library_card_link_url(self):
        return self.find_element(*self._get_a_library_card_link_url_locator)

    @property
    def staff_catalog_title_label(self):
        return self.find_element(*self._staff_catalog_title_label_locator)

    @property
    def staff_list_label(self):
        return self.find_element(*self._staff_list_label_locator)

    @property
    def custom_label_for_other_resource(self):
        return self.find_element(*self._custom_label_for_other_resource_locator)

    @property
    def city_link(self):
        return self.find_element(*self._city_link_locator)

    @property
    def city_name(self):
        return self.find_element(*self._city_name_locator)

    @property
    def other_footer_link(self):
        return self.find_element(*self._other_footer_link_locator)

    @property
    def other_footer_link_text(self):
        return self.find_element(*self._other_footer_link_text_locator)

    @property
    def url_for_support_link(self):
        return self.find_element(*self._url_for_support_link_locator)

    @property
    def support_library_image(self):
        elements = self.find_elements(*self._image_locator)
        return elements[1]

    @property
    def logo_shared_to_social_media(self):
        elements = self.find_elements(*self._image_locator)
        return elements[2]

    @property
    def facebook(self):
        return self.find_element(*self._facebook_locator)

    @property
    def twitter(self):
        return self.find_element(*self._twitter_locator)

    @property
    def flickr(self):
        return self.find_element(*self._flickr_locator)

    @property
    def instagram(self):
        return self.find_element(*self._instagram_locator)

    @property
    def youtube(self):
        return self.find_element(*self._youtube_locator)

    @property
    def pinterest(self):
        return self.find_element(*self._pinterest_locator)

    @property
    def tumblr(self):
        return self.find_element(*self._tumblr_locator)

    @property
    def newsletter(self):
        return self.find_element(*self._newsletter_locator)

    @property
    def enable_make_tags_a_structured_taxonomy(self):
        element = self.find_element(*self._enable_locator)
        try:
            if element.is_enabled():
                return element
        except Exception as exception:
            raise exception

    @property
    def disable_make_tags_a_structured_taxonomy(self):
        element = self.find_element(*self._disable_locator)
        try:
            if element.is_enabled():
                return element
        except Exception as exception:
            raise exception

    @property
    def blog_card_default_image(self):
        elements = self.find_elements(*self._image_locator)
        return elements[3]

    @property
    def news_card_default_image(self):
        elements = self.find_elements(*self._image_locator)
        return elements[4]

    @property
    def select_widget_image(self):
        return SelectWidgetImage(self)
