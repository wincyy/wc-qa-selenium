from selenium.webdriver.common.by import By

from pages.web.wpadmin.v3.create_a_new_card.new_card_base import NewCardBasePage

class CreateNewTwitterCard(NewCardBasePage):

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _tweet_url_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-url']")
    _grab_tweet_info_button_locator = (By.CSS_SELECTOR, "button#js-get-twitter-data")

    @property
    def loaded(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def tweet_url(self):
        return self.find_element(*self._tweet_url_locator)

    @property
    def grab_tweet_info(self):
        return self.find_element(*self._grab_tweet_info_button_locator)
