from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementNotSelectableException
from selenium.webdriver.support.ui import Select

from pages.web.wpadmin.v3.create_a_new_card.new_card_base import NewCardBasePage
from pages.web.components.select_widget_image import SelectWidgetImage

class CreateNewCustomCard(NewCardBasePage):

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _card_image_locator = (By.CSS_SELECTOR, "button#js--open-crop")
    _card_title_url_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-url']")
    _card_description_locator = (By.CSS_SELECTOR, "textarea[data-key='settings-card-description']")
    _resource_type_dropdown_locator = (By.CSS_SELECTOR, "select[data-key='settings-tax-content-type']")

    @property
    def loaded(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def card_image(self):
        return self.find_element(*self._card_image_locator)

    @property
    def card_title_url(self):
        return self.find_element(*self._card_title_url_locator)

    @property
    def card_description_locator(self):
        return self.find_element(*self._card_description_locator)

    @property
    def resource_type_dropdown(self):
        return self.find_element(*self._resource_type_dropdown_locator)

    def select_resource_type(self, resourceType):
        options = ["(no label)", "Catalog Search", "Website", "Article", "Video", "Exhibit", "Archive Collection", "Photo Gallery", "battle"]
        select = Select(self.resource_type_dropdown)
        if resourceType in options:
            select.select_by_visible_text(resourceType)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def select_widget_image(self):
        return SelectWidgetImage(self)
