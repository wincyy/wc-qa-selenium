from selenium.webdriver.common.by import By

from pages.web.wpadmin.v3.create_a_new_card.new_card_base import NewCardBasePage

class CreateNewCatalogCommentCard(NewCardBasePage):

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _catalog_comment_url_locator = (By.CSS_SELECTOR, "input#fm-bw_card_catalog-comment-0-bw_card_url-0")
    _grab_comment_info_button_locator = (By.CSS_SELECTOR, "button#js-get-comment-data")
    _staff_pick_locator = (By.CSS_SELECTOR, "input[data-key='settings-tax-content-type']")
    _community_pick_locator = (By.CSS_SELECTOR, "settings-tax-content-type")

    @property
    def loaded(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def catalog_comment_url(self):
        return self.find_element(*self._catalog_comment_url_locator)

    @property
    def grab_comment_info(self):
        return self.find_element(*self._grab_comment_info_button_locator)

    @property
    def staff_pick_locator(self):
        element = self.find_element(*self._staff_pick_locator)
        try:
            if element.is_enabled():
                return element
        except Exception as exception:
            raise exception

    @property
    def community_pick_locator(self):
        element = self.find_element(*self._community_pick_locator)
        try:
            if element.is_enabled():
                return element
        except Exception as exception:
            raise exception
