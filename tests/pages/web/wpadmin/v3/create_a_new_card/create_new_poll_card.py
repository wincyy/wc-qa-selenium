from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

from pages.web.wpadmin.v3.create_a_new_card.new_card_base import NewCardBasePage
from pages.web.components.select_widget_image import SelectWidgetImage

class CreateNewPollCard(NewCardBasePage):

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _select_a_poll_dropdown_locator = (By.CSS_SELECTOR, "div#fm_bw_card_poll_0_bw_card_gform_id_0_chosen")
    _select_a_poll_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_card_poll_0_bw_card_gform_id_0_chosen > div > ul > li")
    _card_image_locator = (By.CSS_SELECTOR, "button#js--open-crop")
    _link_text_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-call-to-action-text']")
    _link_url_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-call-to-action-url']")

    @property
    def loaded(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def select_a_poll_dropdown(self):
        return self.find_element(*self._select_a_poll_dropdown_locator)

    def select_a_poll(self, poll):
        self.wait.until(EC.visibility_of_all_elements_located(self._select_a_poll_dropdown_results_locator))
        results = self.find_elements(*self._select_a_poll_dropdown_results_locator)

        for index, result in enumerate(results):
            try:
                if result.get_attribute("textContent") == poll:
                    self.wait.until(EC.visibility_of_all_elements_located(self._select_a_poll_dropdown_results_locator))
                    # Redefining temporary list to avoid StaleElementException
                    _ = self.find_elements(*self._select_a_poll_dropdown_results_locator)
                    return _[index]
            except Exception as exception:
                raise exception
        else:
            raise NoSuchElementException

    @property
    def card_image(self):
        return self.find_element(*self._card_image_locator)

    @property
    def link_text(self):
        return self.find_element(*self._link_text_locator)

    @property
    def link_url(self):
        return self.find_element(*self._link_url_locator)

    @property
    def select_widget_image(self):
        return SelectWidgetImage(self)
