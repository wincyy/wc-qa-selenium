from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

from pages.web.wpadmin.v3.create_a_new_card.new_card_base import NewCardBasePage
from pages.web.components.select_widget_image import SelectWidgetImage

class CreateNewOnlineResourceCard(NewCardBasePage):

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _select_an_online_resource_dropdown_locator = (By.CSS_SELECTOR, "div#fm_bw_online_resource_0_bw_online_resource_id_0_chosen")
    _select_an_online_resource_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_online_resource_0_bw_online_resource_id_0_chosen > div > ul > li")
    _grab_info_button_locator = (By.CSS_SELECTOR, "button#js-get-online-resource")
    _card_image_locator = (By.CSS_SELECTOR, "button#js--open-crop")
    _card_title_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-title']")
    _resource_url_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-url']")
    _card_description_locator = (By.CSS_SELECTOR, "textarea[data-key='settings-card-description']")

    @property
    def loaded(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def select_an_online_resource_dropdown(self):
        return self.find_element(*self._select_an_online_resource_dropdown_locator)

    def select_an_online_resource(self, onlineResource):
        self.wait.until(EC.visibility_of_all_elements_located(self._select_an_online_resource_dropdown_results_locator))
        results = self.find_elements(*self._select_an_online_resource_dropdown_results_locator)

        for index, result in enumerate(results):
            try:
                if result.get_attribute("textContent") == onlineResource:
                    self.wait.until(EC.visibility_of_all_elements_located(self._select_an_online_resource_dropdown_results_locator))
                    # Redefining temporary list to avoid StaleElementException
                    _ = self.find_elements(*self._select_an_online_resource_dropdown_results_locator)
                    return _[index]
            except Exception as exception:
                raise exception
        else:
            raise NoSuchElementException

    @property
    def grab_info(self):
        return self.find_element(*self._grab_info_button_locator)

    @property
    def card_image(self):
        return self.find_element(*self._card_image_locator)

    @property
    def card_title(self):
        return self.find_element(*self._card_title_locator)

    @property
    def resource_url(self):
        return self.find_element(*self._resource_url_locator)

    @property
    def card_description(self):
        return self.find_element(*self._card_description_locator)

    @property
    def select_widget_image(self):
        return SelectWidgetImage(self)
