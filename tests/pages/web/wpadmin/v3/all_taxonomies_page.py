from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

from pages.web.staff_base import StaffBasePage
from pages.web.components.card_taxonomies import CardTaxonomies, TaxonomiesTable, Rows

# https://<web-url>/wp-admin/edit-tags.php?<arguments>
class AllTaxonomiesPage(StaffBasePage):                  # Applicable to all card taxonomies
    URL_TEMPLATE = "/wp-admin/edit-tags.php"

    _taxonomy_heading_locator = (By.CLASS_NAME, "wp-heading-inline")

    _add_new_taxonomy_locator = (By.CSS_SELECTOR, ".form-wrap > h2")

    _parent_taxonomy_label_locator = (By.CSS_SELECTOR, "[for='parent']")
    _parent_taxonomy_dropdown_locator = (By.ID, "parent")
    _parent_taxonomy_dropdown_all_locators = (By.CSS_SELECTOR, "#parent [value]")

    _add_new_taxonomy_button_locator = (By.ID, "submit")

    _search_taxonomies_textbox_locator = (By.ID, "tag-search-input")
    _search_taxonomies_button_locator = (By.ID, "search-submit")

    _table_rows_locator = (By.CSS_SELECTOR, "tr[id*='tag-']")

    @property
    def loaded(self):
      try:
            return self.find_element(*self._taxonomy_heading_locator).is_displayed()
      except NoSuchElementException:
            return False

    @property
    def all_taxonomies(self):
       return CardTaxonomies(self)

    @property
    def is_taxonomy_heading_displayed(self):
        return self.find_element(*self._taxonomy_heading_locator)

    @property
    def is_add_new_taxonomy_heading_displayed(self):
        return self.find_element(*self._add_new_taxonomy_locator)

    @property
    def is_parent_taxonomy_label_displayed(self):
        return self.find_element(*self._parent_taxonomy_label_locator)

    def select_parent_taxonomy_dropdown(self, value):
        dropdown_option_locator = self.find_element(*self._parent_taxonomy_dropdown_locator)
        dropdown_option_locator.click()
        options = self.driver.find_elements(*self._parent_taxonomy_dropdown_all_locators)

        found = False
        for option in options:
            if option.text == value:
                option.click()
                found = True
                break
        if found is False:
            raise NoSuchElementException("No option(s) found")

    @property
    def add_new_taxonomy_button(self):
        return self.find_element(*self._add_new_taxonomy_button_locator)

    @property
    def input_search_taxonomies(self):
        return self.find_element(*self._search_taxonomies_textbox_locator)

    @property
    def search_taxonomies_button(self):
        return self.find_element(*self._search_taxonomies_button_locator)

    @property
    def table_actions(self):
       return TaxonomiesTable(self)

    @property
    def taxonomy_rows(self):
        return [Rows(self, element) for element in self.find_elements(*self._table_rows_locator)]
