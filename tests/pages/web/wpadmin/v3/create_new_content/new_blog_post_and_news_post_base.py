from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC

from pages.web.staff_base import StaffBasePage
from pages.web.components.select_widget_image import SelectWidgetImage
from pages.web.components.image_cropper import ImageCropper
from pages.web.components.insert_media import InsertMedia
from pages.web.components.description_area_in_pages_and_posts import Text, Visual

class BlogPostNewsPostBasePage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/post-new.php"

    _title_locator = (By.CSS_SELECTOR, "input#title")
    _add_media_locator = (By.CSS_SELECTOR, "button#insert-media-button")
    _add_form_locator = (By.CSS_SELECTOR, "a#add_gform")

    #Meta Description
    _autofill_meta_description_locator = (By.CSS_SELECTOR, "button#js-autofillexcerpt-button")
    _meta_description_text_locator = (By.CSS_SELECTOR, "textarea#excerpt")

    #Card Information
    _card_image_locator = (By.CSS_SELECTOR, "button#js--open-crop")
    _autofill_text_fields_locator = (By.CSS_SELECTOR, "button#js-autofill-button")
    _card_title_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-title']")
    _card_description_locator = (By.CSS_SELECTOR, "textarea[data-key='settings-card-description']")

    #Description Area
    _visual_locator = (By.CSS_SELECTOR, "button#content-tmce")
    _text_locator = (By.CSS_SELECTOR, "button#content-html")

    #Taxonomies
    _featured_evergreen_checkboxes_locator = (By.CSS_SELECTOR, "span > label")
    _audience_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_audience_category-0']")
    _audience_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bw_audience_category_0_chosen > div > ul > li")
    _related_format_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_related_format-0']")
    _related_format_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bw_related_format_0_chosen > div > ul > li")
    _programs_and_campaigns_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bc_program-0']")
    _programs_and_campaigns_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bc_program_0_chosen > div > ul > li")
    _genre_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_tax_genre-0']")
    _genre_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bw_tax_genre_0_chosen > div > ul > li")
    _topic_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_tax_topic-0']")
    _topic_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bw_tax_topic_0_chosen > div > ul > li")
    _tags_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-post_tag-0']")
    _tags_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_post_tag_0_chosen > div > ul > li")
    _tags_field_search_choices_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_post_tag_0_chosen > ul > li.search-choice > span")
    _free_text_tags_added_list_locator = (By.CSS_SELECTOR, "#post_tag > ul > li")

    #Free Text Tags
    _free_text_tags_input_locator = (By.CSS_SELECTOR, "input#new-tag-post_tag")
    _add_button_locator = (By.CSS_SELECTOR, "input.button.tagadd")

    #Featured Image
    _featured_image_locator = (By.CSS_SELECTOR, "a#set-post-thumbnail")

    #Publish
    _save_draft_locator = (By.CSS_SELECTOR, "input#save-post")
    _preview_locator = (By.CSS_SELECTOR, "a#post-preview")
    _move_to_trash_locator = (By.CSS_SELECTOR, "a[href*='trash']")
    _publish_locator = (By.CSS_SELECTOR, "input#publish")

    @property
    def title(self):
        return self.find_element(*self._title_locator)

    def add_media(self):
        element = self.find_element(*self._add_media_locator)
        self.driver.execute_script("arguments[0].click()", element)

    @property
    def add_form(self):
        return self.find_element(*self._add_form_locator)

    @property
    def autofill_meta_description(self):
        return self.find_element(*self._autofill_meta_description_locator)

    @property
    def meta_description_text(self):
        return self.find_element(*self._meta_description_text_locator)

    @property
    def card_image(self):
        return self.find_element(*self._card_image_locator)

    @property
    def autofill_text_fields(self):
        return self.find_element(*self._autofill_text_fields_locator)

    @property
    def card_title(self):
        return self.find_element(*self._card_title_locator)

    @property
    def card_description(self):
        return self.find_element(*self._card_description_locator)

    @property
    def taxonomies_featured_checkbox(self):
        self.wait.until(EC.presence_of_all_elements_located(self._featured_evergreen_checkboxes_locator))
        element = self.find_elements(*self._featured_evergreen_checkboxes_locator)
        try:
            if element[1].is_enabled():
                return element[1]
        except Exception as exception:
            raise exception

    @property
    def taxonomies_evergreen_checkbox(self):
        self.wait.until(EC.presence_of_all_elements_located(self._featured_evergreen_checkboxes_locator))
        element = self.find_elements(*self._featured_evergreen_checkboxes_locator)
        try:
            if element[2].is_enabled():
                return element[2]
        except Exception as exception:
            raise exception

    def taxonomy(self, taxonomy):
        taxonomies = {
            "audience": self._audience_dropdown_locator,
            "related format": self._related_format_dropdown_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_locator,
            "genre": self._genre_dropdown_locator,
            "topic": self._topic_dropdown_locator,
            "tags": self._tags_dropdown_locator
        }

        return self.find_element(*(taxonomies.get(taxonomy.casefold())))

    def select_taxonomy(self, taxonomy_dropdown, taxonomy_name):
        taxonomies_dropdowns_results = {
            "audience": self._audience_dropdown_results_locator,
            "related format": self._related_format_dropdown_results_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_results_locator,
            "genre": self._genre_dropdown_results_locator,
            "topic": self._topic_dropdown_results_locator,
            "tags": self._tags_dropdown_results_locator
        }

        self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        results = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))

        for index, result in enumerate(results):
            try:
                if result.get_attribute("textContent") == taxonomy_name:
                    self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    # Redefining temporary list to avoid StaleElementException
                    _ = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    return _[index]
            except Exception as exception:
                raise exception
        else:
            raise NoSuchElementException

    @property
    def is_tags_taxonomy_displayed(self):
        try:
            return self.find_element(*self._tags_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    def is_tags_taxonomy_term_selected(self):
        results = self.find_elements(*self._tags_field_search_choices_locator)

        if results is not None:
            if len(results) > 0:
                return True
            else:
                return False
        else:
            raise Exception

    @property
    def is_free_text_tags_field_displayed(self):
        try:
            return self.find_element(*self._free_text_tags_input_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def free_text_tags(self):
        return self.find_element(*self._free_text_tags_input_locator)

    @property
    def add_free_text_tags(self):
        return self.find_element(*self._add_button_locator)

    def are_free_text_tags_added(self):
        tags = self.find_elements(*self._free_text_tags_added_list_locator)

        if tags is not None:
            if len(tags) > 0:
                return True
            else:
                return False
        else:
            raise Exception

    @property
    def save_draft(self):
        return self.find_element(*self._save_draft_locator)

    @property
    def preview(self):
        return self.find_element(*self._preview_locator)

    @property
    def move_to_trash(self):
        return self.find_element(*self._move_to_trash_locator)

    @property
    def publish(self):
        return self.find_element(*self._publish_locator)

    @property
    def select_widget_image(self):
        return SelectWidgetImage(self)

    def taxonomy_terms(self, taxonomy):
        taxonomies_dropdowns_results = {
            "audience": self._audience_dropdown_results_locator,
            "related format": self._related_format_dropdown_results_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_results_locator,
            "genre": self._genre_dropdown_results_locator,
            "topic": self._topic_dropdown_results_locator,
            "tags": self._tags_dropdown_results_locator
        }

        _ = []

        self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        results = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        for index, result in enumerate(results):
            _.append(result.get_attribute("textContent"))

        return _

    @property
    def text(self):
        return self.find_element(*self._text_locator)

    @property
    def visual(self):
        return self.find_element(*self._visual_locator)

    @property
    def text_area(self):
        return Text(self)

    @property
    def visual_area(self):
        return Visual(self)

    @property
    def insert_media(self):
        return InsertMedia(self)

    @property
    def image_cropper(self):
        return ImageCropper(self)

    @property
    def featured_image(self):
        return self.find_element(*self._featured_image_locator)

    def scroll_to_top(self):
        element = self.find_element(*self._title_locator)
        self.driver.execute_script("return arguments[0].scrollIntoView(true);", element)
