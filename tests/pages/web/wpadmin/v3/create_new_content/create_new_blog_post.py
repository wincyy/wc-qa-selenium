from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementNotSelectableException, NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select

from pages.web.wpadmin.v3.create_new_content.new_blog_post_and_news_post_base import BlogPostNewsPostBasePage

class CreateNewBlogPostPage(BlogPostNewsPostBasePage):

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")

    # Author
    _author_dropdown_locator = (By.CSS_SELECTOR, "select#post_author_override")
    _author_dropdown_results_locator = (By.CSS_SELECTOR, "select#post_author_override > option")

    #Blog Categories
    _blog_categories_locator = (By.CSS_SELECTOR, "div#category-all > ul > li > label")

    @property
    def loaded(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def author(self):
        return self.find_element(*self._author_dropdown_locator)

    def select_author_dropdown(self, author):
        select = Select(self.author)
        author_dropdown_results = self.find_elements(*self._author_dropdown_results_locator)

        found = False
        for option in author_dropdown_results:
            if option.text == author:
                select.select_by_visible_text(author)
                found = True
                break

        if found is False:
            raise ElementNotSelectableException("Author not found.")

    def choose_blog_categories(self, blogCategory):
        self.wait.until(EC.visibility_of_all_elements_located(self._blog_categories_locator))
        categories = self.find_elements(*self._blog_categories_locator)

        for category in range(len(categories)):
            try:
                if categories[category].get_attribute("textContent").casefold().strip() == blogCategory.casefold():
                    self.wait.until(EC.visibility_of_all_elements_located(self._blog_categories_locator))
                    categories = self.find_elements(*self._blog_categories_locator)
                    return categories[category]
            except Exception as exception:
                raise exception
        else:
            raise NoSuchElementException
