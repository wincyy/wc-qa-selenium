from selenium.webdriver.common.by import By
from pypom import Region
from selenium.webdriver.common.action_chains import ActionChains

from pages.web.staff_base import StaffBasePage

class AllPagesPage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/edit.php"

    _heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _add_new_page_locator = (By.CSS_SELECTOR, "a.page-title-action")
    _all_filter_locator = (By.CSS_SELECTOR, "a[href*='all_posts']")
    _mine_filter_locator = (By.CSS_SELECTOR, "a[href*='author']")
    _published_filter_locator = (By.CSS_SELECTOR, "a[href*='publish']")
    _drafts_filter_locator = (By.CSS_SELECTOR, "a[href*='draft']")
    _page_builder_filter_locator = (By.CSS_SELECTOR, "a[href*='bbsort']")
    _search_pages_input_locator = (By.CSS_SELECTOR, "#post-search-input")
    _search_pages_locator = (By.CSS_SELECTOR, "#search-submit")
    _page_rows_locator = (By.CSS_SELECTOR, "tr[id*=post-]")

    @property
    def loaded(self):
        return self.find_element(*self._heading_locator)

    @property
    def add_new(self):
        return self.find_element(*self._add_new_page_locator)

    @property
    def all_filter(self):
        return self.find_element(*self._all_filter_locator)

    @property
    def mine_filter(self):
        return self.find_element(*self._mine_filter_locator)

    @property
    def published_filter(self):
        return self.find_element(*self._published_filter_locator)

    @property
    def drafts_filter(self):
        return self.find_element(*self._drafts_filter_locator)

    @property
    def page_builder_filter(self):
        return self.find_element(*self._page_builder_filter_locator)

    @property
    def search_pages_input(self):
        return self.find_element(*self._search_pages_input_locator)

    @property
    def search_pages(self):
        return self.find_element(*self._search_pages_locator)

    @property
    def page_rows(self):
        return [Rows(self, element) for element in self.find_elements(*self._page_rows_locator)]


class Rows(Region):

    _hover_on_title_locator = (By.CSS_SELECTOR, "a.row-title")
    _checkbox_locator = (By.CSS_SELECTOR, "input[type='checkbox'][name*='post']")
    _title_locator = (By.CSS_SELECTOR, "a.row-title")
    _edit_locator = (By.CSS_SELECTOR, "span.edit > a")
    _quick_edit_locator = (By.CSS_SELECTOR, "span > a.editinline")
    _delete_locator = (By.CSS_SELECTOR, "span.trash > a")
    _view_locator = (By.CSS_SELECTOR, "span.view > a")
    _page_builder_locator = (By.CSS_SELECTOR, "span.fl-builder > a")
    _follow_locator = (By.CSS_SELECTOR, "span.ef_follow_link > a")

    def hover_on_title(self):
        element = self.find_element(*self._hover_on_title_locator)
        ActionChains(self.driver).move_to_element(element).perform()

    @property
    def checkbox(self):
        return self.find_element(*self._checkbox_locator)

    @property
    def title(self):
        return self.find_element(*self._title_locator)

    @property
    def edit(self):
        return self.find_element(*self._edit_locator)

    @property
    def quick_edit(self):
        return self.find_element(*self._quick_edit_locator)

    @property
    def delete(self):
        return self.find_element(*self._delete_locator)

    @property
    def view(self):
        return self.find_element(*self._view_locator)

    @property
    def page_builder(self):
        return self.find_element(*self._page_builder_locator)

    @property
    def follow(self):
        return self.find_element(*self._follow_locator)
