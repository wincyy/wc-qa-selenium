from selenium.webdriver.common.by import By
# from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException
from pypom import Region
from pages.core.base import BasePage
from datetime import date
from datetime import timedelta

class Overlay(Region):
    # _root_locator = (By.CLASS_NAME, "add_bib_to_list_overlay")
    # _root_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-container']")
    # # _root_locator = (By.CLASS_NAME, "overlay-inner")
    # # _root_locator = (By.CSS_SELECTOR, "[class='cp-full-screen-overlay simple-genie open']")
    # # _close_icon_locator = (By.CLASS_NAME, "icon-cancel")
    # _close_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-close']")
    # _add_to_list_heading_locator = (By.CSS_SELECTOR, "[class='add_bib_to_list_overlay'] > h2")
    # _list_names_locator = (By.CLASS_NAME, "list_name_column")
    # _create_draft_and_add_locator = (By.CSS_SELECTOR, "a[testid*='add_link_']")
    # _list_actions_locator = (By.CLASS_NAME, "list_actions_column")
    # _only_me_locator = (By.CSS_SELECTOR, "[data-test-id='list-visibility-radio-button-not_visible']")
    # _publish_locator = (By.CSS_SELECTOR, "[data-test-id='publish-list-submit-button']")

    class AddToList(Region):
        _root_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-container']")
        _create_draft_and_add_locator = (By.CSS_SELECTOR, "a[testid*='add_link_']")
        _list_actions_locator = (By.CLASS_NAME, "list_actions_column")
        _close_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-close']")

        @property
        def loaded(self):
            try:
                # return self.find_element(*self._add_to_list_heading_locator).is_displayed()
                return self.find_element(*self._list_actions_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def create_draft_and_add(self):
            return self.find_elements(*self._create_draft_and_add_locator)

        @property
        def list_actions(self):
            return self.find_elements(*self._list_actions_locator)

        @property
        def close(self):
            return self.find_element(*self._close_locator)

    class ReadyToPublish(Region):
        _root_locator = (By.CSS_SELECTOR, "[class='cp-full-screen-overlay simple-genie open']")
        _only_me_locator = (By.CSS_SELECTOR, "[data-test-id='list-visibility-radio-button-not_visible']")
        _publish_locator = (By.CSS_SELECTOR, "[data-test-id='publish-list-submit-button']")
        # _close_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-close']")
        _close_locator = (By.CLASS_NAME, "overlay-close")

        @property
        def loaded(self):
            try:
                # return self.find_element(*self._add_to_list_heading_locator).is_displayed()
                return self.find_element(*self._close_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def only_me(self):
            return self.find_element(*self._only_me_locator)

        @property
        def publish(self):
            return self.find_element(*self._publish_locator)

        @property
        def close(self):
            return self.find_element(*self._close_locator)

    class AddAComment(Region):
        _root_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-container']")
        _close_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-close']")
        _edit_comment_locator = (By.ID, "edit_comment")
        _post_comment_locator = (By.NAME, "commit")
        _cancel_locator = (By.CLASS_NAME, "link_cancel")

        @property
        def loaded(self):
            try:
                return self.find_element(*self._close_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def close(self):
            return self.find_element(*self._close_locator)

        @property
        def edit_comment(self):
            return self.find_element(*self._edit_comment_locator)

        @property
        def post_comment(self):
            return self.find_element(*self._post_comment_locator)

        @property
        def cancel(self):
            return self.find_element(*self._cancel_locator)

    class AddTags(Region):
        _root_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-container']")
        _add_tags_heading_locator = (By.CSS_SELECTOR, "[class='tags_title_wrapper'] > h3")
        _genre_field_locator = (By.CSS_SELECTOR, "[testid='field_tag_genre']")
        _post_tags_locator = (By.NAME, "commit")
        _remove_tags_locator = (By.CSS_SELECTOR, "[data-js='remove_tag']")

        @property
        def loaded(self):
            try:
                return self.find_element(*self._add_tags_heading_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def genre_field(self):
            return self.find_element(*self._genre_field_locator)

        @property
        def post_tags(self):
            return self.find_element(*self._post_tags_locator)

        @property
        def remove_tags(self):
            return self.find_elements(*self._remove_tags_locator)

        @property
        def genre_field(self):
            return self.find_element(*self._genre_field_locator)

    class Availability(Region):
        _root_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-container']")
        _heading_locator = (By.ID, "content-start")
        _table_locator = (By.CSS_SELECTOR, "th[scope='col']")

        @property
        def loaded(self):
            try:
                return self.find_element(*self._heading_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def heading(self):
            return self.find_element(*self._heading_locator)

        @property
        def library_table(self):
            return self.find_element(*self._table_locator)

        @property
        def is_availability_status_displayed(self):
            return self.find_element(*self._table_locator).is_displayed()

    @property
    def ready_to_publish(self):
        return self.ReadyToPublish(self)

    @property
    def add_a_comment(self):
        return self.AddAComment(self)

    @property
    def add_tags(self):
        return self.AddTags(self)

    @property
    def availability(self):
        return self.Availability(self)

    @property
    def login(self):
        return self.Login(self)

    class Login(Region):
        _root_locator = (By.CSS_SELECTOR, "[data-test-id='modal-container']")
        _username_form_locator = (By.CSS_SELECTOR, "[testid='field_username']")
        _pin_form_locator = (By.CSS_SELECTOR, "[testid='field_userpin']")
        _login_button_locator = (By.CSS_SELECTOR, "[testid='button_login']")

        @property
        def loaded(self):
            try:
                return self.find_element(*self._login_button_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def barcode_input_form(self):
            return self.find_element(*self._username_form_locator)

        @property
        def pin_input_form(self):
            return self.find_element(*self._pin_form_locator)

        @property
        def login_button(self):
            return self.find_element(*self._login_button_locator)

    @property
    def search_page_login(self):
        return self.SearchPageLogin(self)

    class SearchPageLogin(Region):
        _v2_overlay_close = (By.CSS_SELECTOR, "['data-test-id=overlay-close']")
        _v2_username_field = (By.ID, "login-username")
        _v2_login_submit_button = (By.CSS_SELECTOR, ".cp-btn.btn.cp-primary-btn.btn-primary")
        _v2_pin_field = (By.ID, "login-password")

        @property
        def username_field(self):
            return self.find_element(*self._v2_username_field)

        @property
        def username_is_displayed(self):
            try:
                return self.find_element(*self._v2_username_field).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def pin_field(self):
            return self.find_element(*self._v2_pin_field)

        @property
        def login_submit_button(self):
            return self.find_elements(*self._v2_login_submit_button)

        def log_in(self, username, password):
            overlay = Overlay(self)
            overlay.search_page_login.wait.until(lambda a: overlay.search_page_login.login_submit_button[1].is_displayed())
            overlay.search_page_login.username_field.send_keys(username)
            overlay.search_page_login.pin_field.send_keys(password)
            overlay.search_page_login.login_submit_button[1].click()

    @property
    def bib_cancel(self):
        return self.CancelHoldBibPage(self)

    class CancelHoldBibPage(Region):
        _root_locator = (By.CSS_SELECTOR, "[data-test-id='modal-container']")
        _confirm_cancel_hold_locator = (By.CSS_SELECTOR, "[data-test-id='button-confirm-continue']")

        @property
        def confirm_cancel_hold(self):
            return self.find_element(*self._confirm_cancel_hold_locator)

        @property
        def is_confirm_cancel_hold_displayed(self):
            try:
                return self.find_element(*self._confirm_cancel_hold_locator).is_displayed()
            except NoSuchElementException:
                return False

    @property
    def overdrive_checkout(self):
        return self.OverdriveBibPageCheckout(self)

    class OverdriveBibPageCheckout(Region):
        _root_locator = (By.CSS_SELECTOR, "div.modal-content")
        _digital_checkout_button_locator = (By.CSS_SELECTOR, "[testid='place_digital_checkout']")

        @property
        def loaded(self):
            try:
                return self.find_element(*self._digital_checkout_button_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def checkout_digital_item(self):
            return self.find_element(*self._digital_checkout_button_locator)

        @property
        def is_checkout_digital_item_displayed(self):
            try:
                return self.find_element(*self._digital_checkout_button_locator)
            except NoSuchElementException:
                return False

    @property
    def suspend_end_date(self):
        return self.SuspendEndDate(self)

    class SuspendEndDate(Region):
        #_root_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-body']")
        _close_overlay_locator = (By.CSS_SELECTOR, "button.cp-btn.btn.cp-text-button.item-column")
        _next_date = date.today() + timedelta(days=2)
        _next_date_string = _next_date.strftime("%A, %-d %B, %Y")
        _next_date_selector = "td[aria-label='{}']".format(_next_date_string)

        _next_date_locator = (By.CSS_SELECTOR, _next_date_selector)

        @property
        def displayed(self):
            try:
                _close_modal_locator = self.find_elements(*self._close_overlay_locator)
                return _close_modal_locator[3].is_displayed()
            except NoSuchElementException:
                return False

        @property
        def suspend_end_date(self):
            _suspend_end_dates = self.find_elements(*self._next_date_locator)
            return _suspend_end_dates[1]




    @property
    def submit_suggestion(self):
        return self.SubmitSuggestion(self)

    class SubmitSuggestion(Region):
        _root_locator = (By.CSS_SELECTOR, "[class='cp_add_suggestion_overlay']")
        _title_locator = (By.CSS_SELECTOR, "[data-test-id='suggestion-title']")
        _author_locator = (By.CSS_SELECTOR, "[data-test-id='suggestion-author']")
        _publication_year_locator = (By.CSS_SELECTOR, "[data-test-id='suggestion-publication-year']")
        _next_step_locator = (By.CSS_SELECTOR, "[data-test-id='suggestion-next-step']")
        _close_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-close']")
        _format_locator = (By.CSS_SELECTOR, "[data-test-id='suggestion-format']")
        _audience_locator = (By.CSS_SELECTOR, "[data-test-id='suggestion-audience']")
        _content_locator = (By.CSS_SELECTOR, "[data-test-id='suggestion-fiction-type']")
        _dropdown_list_item_locator = (By.CSS_SELECTOR, "li[data-original-index]")
        _submit_button_locator = (By.CSS_SELECTOR, "[data-test-id='suggestion-submit']")

        @property
        def title(self):
            return self.find_element(*self._title_locator)

        @property
        def author(self):
            return self.find_element(*self._author_locator)

        @property
        def publication_year(self):
            return self.find_element(*self._publication_year_locator)

        @property
        def next_step(self):
            return self.find_element(*self._next_step_locator)

        @property
        def close(self):
            return self.find_element(*self._close_locator)

        @property
        def is_loaded(self):
            try:
                return self.find_element(*self._next_step_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def format(self):
            return self.find_element(*self._format_locator)

        @property
        def audience(self):
            return self.find_element(*self._audience_locator)

        @property
        def content(self):
            return self.find_element(*self._content_locator)

        @property
        def is_dropdown_displayed(self):
            try:
                return self.find_element(*self._audience_locator).is_displayed()
            except NoSuchElementException:
                return False

        def select_from_dropdown(self, list, value):
            if list == 'format':
                dropdown = self.format
            elif list == 'audience':
                dropdown = self.audience
            elif list == 'content':
                dropdown = self.content
            else:
                raise NoSuchElementException("List name given is not valid for this page.")
            dropdown.click()
            options = dropdown.find_elements(*self._dropdown_list_item_locator)
            for option in options:
                if option.text == value:
                    option.click()
                    break

        @property
        def submit_button(self):
            return self.find_element(*self._submit_button_locator)

        @property
        def is_submit_button_displayed(self):
            try:
                return self.find_element(*self._submit_button_locator).is_displayed()
            except NoSuchElementException:
                return False



    @property
    def review_suggested_purchases(self):
        return self.ReviewSuggestedPurchases(self)

    class ReviewSuggestedPurchases(Region):
        _root_locator = (By.CSS_SELECTOR, "[data-test-id='overlay-body-ready']")
        _suggested_purchase_title_locator = (By.CSS_SELECTOR, "[data-test-id='reviewed-suggestion-title']")
        _suggestion_custom_response_radio_locator = (By.CSS_SELECTOR, "[data-test-id='status_type_FREE_FORM_STATUS']")
        _suggestion_custom_response_textbox_locator = (By.CSS_SELECTOR, "[data-test-id='suggestion-free-form-response-textarea']")
        _suggestion_overlay_deny_button_locator = (By.CSS_SELECTOR, "[data-test-id='suggestion-overlay-deny-button']")

        @property
        def suggested_purchase_title(self):
            return self.find_element(*self._suggested_purchase_title_locator)

        @property
        def is_deny_suggestion_loaded(self):
            try:
                return self.find_element(*self._suggested_purchase_title_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def suggestion_custom_response_radio(self):
            return self.find_element(*self._suggestion_custom_response_radio_locator)

        @property
        def suggestion_custom_response_textbox(self):
            return self.find_element(*self._suggestion_custom_response_textbox_locator)

        @property
        def is_suggestion_custom_response_textbox_loaded(self):
            try:
                return self.find_element(*self._suggestion_custom_response_textbox_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def suggestion_overlay_deny_button(self):
            return self.find_element(*self._suggestion_overlay_deny_button_locator)
