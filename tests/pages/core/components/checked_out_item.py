from pypom import Region
from selenium.webdriver.common.by import By


class CheckedOutItem(Region):
    '''/v2/checkedout page'''

    _return_now_locator = (By.CSS_SELECTOR, ".cp-primary-btn.btn-primary")
    _bib_title_locator =  (By.CSS_SELECTOR, "span.title-content")

    @property
    def return_now(self):
        return self.find_element(*self._return_now_locator)

    @property
    def bib_title(self):
        return self.find_element(*self._bib_title_locator)
