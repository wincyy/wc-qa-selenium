from selenium.webdriver.common.by import By
from .base import BasePage

class LoginPage(BasePage):

    _username_or_barcode_locator = (By.CSS_SELECTOR, "[testid='field_username']")
    _password_locator = (By.CSS_SELECTOR, "[testid='field_userpin']")
    _log_in_button_locator = (By.CSS_SELECTOR, "[testid='button_login']")

    @property
    def username_or_barcode(self):
        return self.find_element(*self._username_or_barcode_locator)

    @property
    def password(self):
        return self.find_element(*self._password_locator)

    @property
    def log_in_button(self):
        return self.find_element(*self._log_in_button_locator)
