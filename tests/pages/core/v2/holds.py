from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

from pages.core.components.overlay import Overlay
from pages.core.base import BasePage
from pages.core.v2.on_holds_item import OnHoldItem
from pypom import Page, Region

class HoldsPage(BasePage):
    URL_TEMPLATE = "/v2/holds/not_yet_available"

    _heading_locator = (By.CSS_SELECTOR, ".cp-core-external-header")  # "On Hold" heading
    _on_hold_item_locator = (By.CSS_SELECTOR, "div.cp-batch-actions-list-item")
    _empty_page_locator = (By.CLASS_NAME, ".cp-borrowing-empty-content-panel.cp-holds-empty-content-panel")
    _holds_list_locator = (By.CLASS_NAME, ".cp-bib-list")
    _confirm_cancel_hold_locator = (By.CSS_SELECTOR, "[data-test-id='confirm-cancel-hold']")
    _holds_notification_success_locator = (By.CSS_SELECTOR, ".alert.cp-alert.alert-success.cp-alert-success")
    _bib_title_locator = (By.CSS_SELECTOR, "[data-key='bib-title']")
    _print_view_button_locator = (By.CSS_SELECTOR, "[data-test-id='print-view-button']")
    _cancelled_items_locator = (By.CSS_SELECTOR, ".cp-review-button.cancelled-button")
    _single_click_holds_locator = (By.CSS_SELECTOR, ".cp-single-click-holds-status-button")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def items(self):
        return [OnHoldItem(self, element) for element in self.find_elements(*self._on_hold_item_locator)]

    @property
    def holds_list(self):
        return self.find_element(*self._on_hold_item_locator)

    @property
    def holds_list_is_displayed(self):
        try:
            return self.find_element(*self._on_hold_item_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_empty_page_displayed(self):
        try:
            return self.find_element(*self._empty_page_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def holds_notification(self):
        return self.find_element(*self._holds_notification_success_locator)

    @property
    def is_success_notification_displayed(self):
        try:
            return self.find_element(*self._holds_notification_success_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def print_view(self):
        return self.find_element(*self._print_view_button_locator)

    @property
    def cancelled_holds(self):
        return self.find_element(*self._cancelled_items_locator)

    @property
    def single_click_holds(self):
        return self.find_element(*self._single_click_holds_locator)

    @property
    def overlay(self):
        return Overlay(self)


