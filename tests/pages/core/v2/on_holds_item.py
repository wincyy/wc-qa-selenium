from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from pypom import Region
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class OnHoldItem(Region):

    _pause_locator = (By.CSS_SELECTOR, ".cp-pause-hold-button")
    _cancel_locator = (By.CSS_SELECTOR, ".cp-cancel-hold-button")
    _suspend_end_date_locator = (By.CSS_SELECTOR, "[data-test-id='suspend-end-date-picker']")
    _pause_holds_locator = (By.CSS_SELECTOR, "[data-test-id='edit-suspend-date-submit-button']")
    _cancel_holds_locator = (By.CSS_SELECTOR, "[data-test-id='button-confirm-continue']")
    _pickup_branch_locator = (By.CSS_SELECTOR, "[data-test-id='single-click-holds-location'] .selected-value")
    _confirm_cancel_hold_locator = (By.CSS_SELECTOR, "[data-test-id='confirm-cancel-hold']")
    _bib_title_locator = (By.CSS_SELECTOR, "[data-key='bib-title']")
    _item_detail_locator = (By.CSS_SELECTOR, "[data-key='bib-availability-link']")
    _pick_an_end_date_locator = (By.CSS_SELECTOR, "div.cp-accessible-date-picker.end-date")
    _pick_a_start_date_locator = (By.CSS_SELECTOR, "div.cp-accessible-date-picker.start-date")
    _confirm_pause_hold_locator = (By.CSS_SELECTOR, "button.cp-btn.btn.cp-primary-btn.btn-primary.pull-right")
    _paused_until_locator = (By.CSS_SELECTOR, "button.cp-btn.btn.cp-text-button.holds-pause-date-link-button")
    _item_status_locator = (By.CLASS_NAME, "cp-hold-status-icon")
    #TODO
    ''''/v2/holds add to shelf require data-keys added, add to shelf selectors are not
    unique enough to locate currently '''
    _default_shelf_locator = (By.CSS_SELECTOR, "")

    @property
    def cancel(self):
        return self.find_element(*self._cancel_locator)

    @property
    def confirm(self):
        return self.find_element(*self._confirm_cancel_hold_locator)

    @property
    def bib_title(self):
        return self.find_element(*self._bib_title_locator)

    @property
    def item_details(self):
        return self.find_element(*self._item_detail_locator)

    @property
    def suspend(self):
        return self.find_element(*self._pause_holds_locator)

    @property
    def pickup_at(self):
        return self.find_element(*self._pickup_branch_locator)

    @property
    def default_shelf(self):
        return self.find_element(*self._default_shelf_locator)

    @property
    def suspend_end_date(self):
        return self.find_element(*self._suspend_end_date_locator)

    @property
    def pause_hold(self):
        return self.find_element(*self._pause_locator)

    @property
    def pick_an_end_date(self): #
        return self.find_element(*self._pick_an_end_date_locator)

    @property
    def is_pick_an_end_date_displayed(self):
        try:
            return self.find_element(*self._pick_an_end_date_locator)
        except NoSuchElementException:
            False

    @property
    def confirm_pause_hold(self):
        return self.find_element(*self._confirm_pause_hold_locator)

    @property
    def is_confirm_pause_hold_displayed(self):
        try:
            return self.find_element(*self._confirm_pause_hold_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def paused_until(self):
        return self.find_element(*self._paused_until_locator)

    @property
    def item_status(self):
        return self.find_element(*self._item_status_locator)
