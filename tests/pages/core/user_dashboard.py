from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
from .base import BasePage
from pages.core.components.my_collections import MyCollectionsComponent
from pages.core.components.recent_activity import RecentActivity
from pages.core.components.overlay import Overlay

# https://<library>.<environment>.bibliocommons.com/user_dashboard
class UserDashboardPage(BasePage):

    URL_TEMPLATE = "/user_dashboard"
    _submit_suggestion_locator = (By.CSS_SELECTOR, "[data-js='cp_add_suggestion_overlay']")
    _suggest_for_purchase_link_locator = (By.CSS_SELECTOR, "[class='my_suggested_purchases panel panel-default'] > a ")

    @property
    def my_collections(self):
        return MyCollectionsComponent(self)

    @property
    def recent_activity(self):
        return RecentActivity(self)

    @property
    def submit_suggestion(self):
        return self.find_element(*self._submit_suggestion_locator)

    @property
    def is_submit_suggestion_displayed(self):
        try:
            return self.find_element(*self._submit_suggestion_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def suggest_for_purchase_link(self):
        return self.find_element(*self._suggest_for_purchase_link_locator)

    @property
    def is_suggest_for_purchase_link_displayed(self):
        try:
            return self.find_element(*self._suggest_for_purchase_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def overlay(self):
        return Overlay(self)
