from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from .base import BasePage

class ListEditorPage(BasePage):

    _list_form_locator = (By.CSS_SELECTOR, "[data-test-id='user-list-editor']")
    _guides_and_recos_list_locator = (By.CSS_SELECTOR, "[data-test-id='list-type-other']")
    _if_you_liked_list_locator = (By.CSS_SELECTOR, "[data-test-id='list-type-read_a_like']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._list_form_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def guides_and_recos_list(self):
        return self.find_element(*self._guides_and_recos_list_locator)

    @property
    def if_you_liked_list(self):
        return self.find_element(*self._if_you_liked_list_locator)
