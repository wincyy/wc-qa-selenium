import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from pages.core.account import AccountPage
from pages.core.pickup_location import PickupLocationPage
from pages.core.v2.holds import HoldsPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46020: Enable one-click hold and place hold")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46020", "TestRail")
class TestC46020:
    def test_C46020(self):
        self.base_url = "https://epl.demo.bibliocommons.com"
        
        home_page = HomePage(self.driver, self.base_url).open()
        home_page.header.log_in("21221011111111", "1234")
        home_page.header.login_state_user_logged_in.click()
        home_page.header.my_settings.click()
        account_page = AccountPage(self.driver)
        account_page.change_pickup_location.click()
        pickup_location_page = PickupLocationPage(self.driver)
        pickup_location_page.single_click_holds_toggle.click()
        pickup_location_page.save_changes.click()
        search_term = "Doctor You"
        search_results_page = pickup_location_page.header.search_for(search_term)
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.search_result_items[0].place_hold.click()
        search_results_page.wait.until(lambda s: search_results_page.search_result_items[0].is_alert_displayed)
        search_results_page.header.login_state_user_logged_in.click()
        search_results_page.header.on_hold.click()
        holds_page = HoldsPage(self.driver)
        holds_page.wait.until(lambda s: (len(holds_page.items) > 0))
        holds_page.items[0].pickup_at.get_attribute('innerHTML').should.equal("Castle Downs")

        # Cancel the hold placed and change back Single Click Holds settings:
        holds_page = HoldsPage(self.driver)
        holds_page.items[0].cancel.click()
        holds_page.items[0].confirm.click()
        holds_page.header.login_state_user_logged_in.click()
        holds_page.header.my_settings.click()
        pickup_location_page = PickupLocationPage(self.driver, self.base_url).open()
        pickup_location_page.single_click_holds_toggle.click()
        pickup_location_page.save_changes.click()
