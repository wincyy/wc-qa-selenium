import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.bib import BibPage
from pages.core.account_username import AccountUsernamePage
from utils.bc_api_gateway import BCAPIGatewayBibs
from utils.bc_test_connector import TestConnector

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46962: Log in with US child account")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/46962", "TestRail")
class TestC46962:
    def test_C46962(self):
        user = TestConnector.User(adult = False).create(self.driver)

        # Use the API gateway to search for a random book (BK) Bib:
        bibs = BCAPIGatewayBibs(configuration.system.base_url).search(term = 'a')
        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = bibs[0].id).open()
        # "Add a comment" option should exist while browsing anonymously:
        bib_page.community_activity.is_add_comment_displayed.should.be.true
        bib_page.header.log_in(user.barcode, user.pin)
        # Since we're now logged in with the child account, "Add a comment" option should be gone:
        bib_page.community_activity.is_add_comment_displayed.should.be.false
        # On the account/username page, there should be no "username" text field displayed:
        account_username_page = AccountUsernamePage(self.driver, configuration.system.base_url).open()
        account_username_page.is_username_displayed.should.be.false
