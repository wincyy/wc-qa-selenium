import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system
import configuration.user

from pages.core.bib import BibPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C47129: Add a rating")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C47129", "TestRail")
class TestC47129:
    def test_C47129(self):
        self.item_id = "719979001"

        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        bib_page.header.log_in(configuration.user.name, configuration.user.password)
