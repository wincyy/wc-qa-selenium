# from mamba import description, before, after, it
# from selenium import webdriver
# from configuration import chrome
# from pages.core.home import HomePage
# import sure
#
# with description("Core Automation Candidates:") as self:
#     with before.all:
#         self.driver = WebDriver().instance
#
#     with after.all:
#         self.driver.quit()
#
#     with it("C47057: Empty Results Page, and PIN too"):
import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C47057: Empty Results Page, and PIN too")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C47057", "TestRail")
class TestC47057:
    def test_C47057(self):
        base_url = "https://chipublib.demo.bibliocommons.com/"
        
        homepage = HomePage(self.driver, base_url).open()
        search_results_page = homepage.header.search_for("Harry Potter and the Prisoner of Azkaban")
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.filter_search_results("Printed Music").click()
        search_results_page.wait.until(lambda w: search_results_page.filter_search_results("Printed Music").is_enabled())
        search_results_page.wait.until(lambda w: search_results_page.active_filter_toggle_disabled.is_displayed())
        search_results_page.active_filter_toggle_disabled.click()
        search_results_page.header.clear_search()
        search_results_page.header.search_for("Franz Kafka Castle")
        search_results_page.wait.until(lambda s: search_results_page.empty_search_result.is_displayed)
        search_results_page.clear_filters_empty_results.click()
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        len(search_results_page.search_result_items).should.be.greater_than(0)
        search_results_page.search_title.text.should.equal("Keyword search: Franz Kafka Castle")
