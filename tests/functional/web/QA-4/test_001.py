import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system
import configuration.user

from pages.web.wpadmin.login import LoginPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("")
@allure.testcase("", "TestRail")

class Test001:
    def test_001(self):
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.name_web, configuration.user.password_web)
        login_page.wait.until(lambda s: login_page.wpheader.is_wp_admin_header_displayed)
        login_page.is_v3_landing_page_displayed.should.be.true
