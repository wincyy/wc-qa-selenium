import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system
import configuration.user
from pages.web.staff_base import StaffBasePage
from pages.web.wpadmin.login import LoginPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
# @allure.title("")
# @allure.testcase("", "TestRail")
class Test004:
    def test_004(self):

        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.name_lib_admin, configuration.user.pw_lib_admin)
        login_page.wait.until(lambda s: login_page.wpheader.is_wp_admin_header_displayed)

        staff_base_page = StaffBasePage(self.driver, configuration.system.base_url_web + "v3-page").open()
        staff_base_page.wpheader.my_sites.click()
        staff_base_page.wpheader.move_to_element(staff_base_page.wpheader.my_sites)
        staff_base_page.wpheader.wait.until(lambda s: staff_base_page.wpheader.is_my_sites_submenu_displayed)
        if staff_base_page.wpheader.is_my_sites_network_admin_locator_displayed:
            staff_base_page.wpheader.move_to_element(staff_base_page.wpheader.my_sites_network_admin)
            staff_base_page.wpheader.wait.until(lambda s: staff_base_page.wpheader.is_my_sites_network_admin_submenu_displayed)
            staff_base_page.wpheader.my_sites_network_admin_themes.click()

        staff_base_page.wpheader.move_to_element(staff_base_page.wpheader.my_sites)
        staff_base_page.wpheader.wait.until(lambda s: staff_base_page.wpheader.is_my_sites_submenu_displayed)
        if staff_base_page.wpheader.is_my_sites_list_displayed:
            staff_base_page.wpheader.wait.until(lambda s: staff_base_page.wpheader.is_my_sites_submenu_displayed)
            staff_base_page.wpheader.move_to_element(staff_base_page.wpheader.my_sites_first)
            staff_base_page.wpheader.wait.until(lambda s: staff_base_page.wpheader.is_my_sites_first_submenu_displayed)
            staff_base_page.wpheader.my_sites_submenu_new_post.click()

        staff_base_page.wpheader.my_site_name.click()
        staff_base_page.wpheader.move_to_element(staff_base_page.wpheader.my_site_name)
        staff_base_page.wpheader.wait.until(lambda s: staff_base_page.wpheader.is_my_site_name_submenu_displayed)
        staff_base_page.wpheader.my_site_themes.click()

        staff_base_page.wpheader.add_new_content.click()
        staff_base_page.wpheader.move_to_element(staff_base_page.wpheader.add_new_content)
        staff_base_page.wpheader.wait.until(lambda s: staff_base_page.wpheader.is_add_new_content_submenu_displayed)
        len(staff_base_page.wpheader.add_new_content_submenu_list).should.equal(10)
        staff_base_page.wpheader.add_new_content_submenu_list[0].text.should.match(
            staff_base_page.wpheader.add_new_card.text)
        staff_base_page.wpheader.add_new_content_submenu_list[1] == staff_base_page.wpheader.add_new_custom_pg
        staff_base_page.wpheader.add_new_faq.click()

        staff_base_page.wpheader.my_account.click()
        staff_base_page.wpheader.move_to_element(staff_base_page.wpheader.my_account)
        staff_base_page.wpheader.wait.until(lambda s: staff_base_page.wpheader.is_my_account_submenu_displayed)
        staff_base_page.wpheader.my_account_log_out.click()

        staff_base_page = StaffBasePage(self.driver, configuration.system.base_url_web + "v3-page").open()
        staff_base_page.wpheader.is_wp_admin_header_displayed.should.be.false