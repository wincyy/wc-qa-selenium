import pytest
import allure
import sure
import os
import sys
sys.path.append('tests')
import configuration.system
import configuration.user

from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from utils.image_download_helper import ImageDownloadHelper

@pytest.mark.usefixtures('selenium_setup_and_teardown')
# @allure.title("")
# @allure.testcase("", "TestRail")
class Test002:
    def test_002(self):
        image_download_helper = ImageDownloadHelper()
        image_download_helper.download_image("https://bit.ly/2TUV5oS")
        login_page = LoginPage(self.driver, "https://calgary-stage.bibliocms.com/").open()
        login_page.log_in(configuration.user.name_web, configuration.user.password_web)
        settings_general_tab = SettingsGeneralTabPage(self.driver, "https://calgary-stage.bibliocms.com/", page='bibliocommons-settings', tab='generic').open()
        settings_general_tab.loaded
        settings_general_tab.get_a_library_card_image.click()
        settings_general_tab.select_widget_image.upload_image.send_keys(os.getcwd() + "/image.jpg")
        settings_general_tab.select_widget_image.add_image_to_widget_button.click()
        settings_general_tab.support_library_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.add_image_to_widget_button.click()
        settings_general_tab.blog_card_default_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.add_image_to_widget_button.click()
        settings_general_tab.news_card_default_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.add_image_to_widget_button.click()
        settings_general_tab.save_changes.click()
        settings_general_tab.open()
        settings_general_tab.loaded

        #Asserting that image was added to the fields
        settings_general_tab.get_a_library_card_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.title.get_attribute("value").should.be.equal("image")
        settings_general_tab.select_widget_image.close_button.click()
        settings_general_tab.support_library_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.title.get_attribute("value").should.be.equal("image")
        settings_general_tab.select_widget_image.close_button.click()
        settings_general_tab.blog_card_default_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.title.get_attribute("value").should.be.equal("image")
        settings_general_tab.select_widget_image.close_button.click()
        settings_general_tab.news_card_default_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.title.get_attribute("value").should.be.equal("image")

        #delete the image locally and from Stage
        settings_general_tab.select_widget_image.delete_image_link.click()
        settings_general_tab.select_widget_image.accept_alert_to_delete_image()
        settings_general_tab.select_widget_image.close_button.click()
        image_download_helper.delete_downloaded_image("image.jpg")
