import pytest
import allure
import os
import sure
from mimesis import Text
import sys
sys.path.append('tests')

from utils.image_download_helper import ImageDownloadHelper
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_new_content.create_new_blog_post import CreateNewBlogPostPage
from pages.web.staff_base import StaffBasePage
from pages.web.components.page_builder.page_builder import PageBuilderHeader, BuilderPanel
from pages.web.components.page_builder.page_builder_modules import SingleCard
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_blog_posts import AllBlogPostsPage

base_url = "http://chicago.local.bibliocms.com/"
page = "bibliocommons-settings"

taxonomies = ['Audience', 'Related Format', 'Programs and Campaigns', 'Genre']
taxonomies_terms = []

_ = Text('en').words(quantity=3)
#converting list to a string
blog_title = ''.join(_)

_ = Text('en').words(quantity=3)
#converting list to a string
page_title = ''.join(_)

@pytest.fixture(scope='class')
def login_and_setup(request, selenium_setup_and_teardown):
    driver = request.cls.driver

    login_page = LoginPage(driver, base_url).open()
    login_page.log_in("admin", "password")
    settings_system_settings_tab = SettingsSystemSettingsTabPage(driver, base_url, page=page, tab='system').open()
    settings_system_settings_tab.v3_status_implementing.click()
    settings_system_settings_tab.save_changes.click()
    settings_system_settings_tab.loaded

    new_blog_post = CreateNewBlogPostPage(driver, base_url, post_type='post').open()
    new_blog_post.loaded

    # Creating a dynamic list of taxonomy terms to select in the test
    for index, taxonomy in enumerate(taxonomies):
        new_blog_post.taxonomy(taxonomy).click()
        taxonomies_terms.append(new_blog_post.taxonomy_terms(taxonomy)[0])
        if index == len(taxonomies) - 1:
            break
        new_blog_post.taxonomies_evergreen_checkbox.click()

@pytest.mark.usefixtures('login_and_setup')
class Tests:
    @allure.title("C69745: Blog Post - Create New")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69745", "TestRail")
    def test_C69745(self):
        image_download_helper = ImageDownloadHelper()
        image_download_helper.download_image("https://bit.ly/2EdPMqG")

        new_blog_post = CreateNewBlogPostPage(self.driver, base_url, post_type='post').open()
        new_blog_post.loaded
        new_blog_post.title.send_keys(blog_title)
        new_blog_post.choose_blog_categories("Uncategorized").click()
        new_blog_post.visual.click()
        visual = new_blog_post.visual_area

        #Adding a FAQ widget
        visual.related_faq_button()
        visual.related_faq.select_category("Most Recent")
        visual.related_faq.insert_shortcode.click()
        visual.is_related_faq_form_visible.should.be.false

        #Adding a location widget
        visual.add_library()
        visual.add_library_locations.choose_library_locations("Albany Park").click()
        visual.add_library_locations.choose_library_locations("Archer Heights").click()
        visual.add_library_locations.insert_into_post.click()
        visual.is_add_library_locations_form_visible.should.be.false

        new_blog_post.add_media()
        new_blog_post.insert_media.upload_image.send_keys(os.getcwd() + "/image.jpg")
        new_blog_post.insert_media.add_image_to_widget_button.click()
        new_blog_post.autofill_text_fields.click()
        new_blog_post.taxonomies_featured_checkbox.click()
        new_blog_post.taxonomies_evergreen_checkbox.click()
        new_blog_post.card_image.click()
        new_blog_post.select_widget_image.select_image_from_list(0).click()
        new_blog_post.select_widget_image.add_image_to_widget_button.click()
        new_blog_post.image_cropper.is_cropper_modal_visible.should.be.true
        new_blog_post.image_cropper.is_crop_one_visible.should.be.true
        new_blog_post.image_cropper.is_cropper_box_visible.should.be.true
        new_blog_post.image_cropper.image_one_crop("400px", "200px")
        new_blog_post.image_cropper.crop_image.click()
        new_blog_post.image_cropper.next.click()
        new_blog_post.image_cropper.is_cropper_box_visible.should.be.true
        new_blog_post.image_cropper.is_crop_one_visible.should.be.false
        new_blog_post.image_cropper.is_crop_two_visible.should.be.true
        new_blog_post.image_cropper.image_two_crop("200px", "200px")
        new_blog_post.image_cropper.crop_image.click()
        new_blog_post.image_cropper.done.click()
        new_blog_post.image_cropper.is_cropper_modal_not_visible.should.be.true
        new_blog_post.featured_image.click()
        new_blog_post.select_widget_image.select_image_from_list(0).click()
        new_blog_post.select_widget_image.add_image_to_widget_button.click()

        for index, taxonomy in enumerate(taxonomies):
            new_blog_post.taxonomy(taxonomies[index]).click()
            new_blog_post.select_taxonomy(taxonomies[index], taxonomies_terms[index]).click()

        new_blog_post.scroll_to_top()
        new_blog_post.publish.click()
        new_blog_post.loaded
        new_blog_post.text.click()
        _ = ["img", "relatedfaqs", "library_location"]
        for index, information in enumerate(_):
            new_blog_post.text_area.text_body.get_attribute("value").should.contain(information)

        image_download_helper.delete_downloaded_image("image.jpg")

        #Opening a new tab
        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        #Creating a new page
        create_new_page = CreateNewPagePage(self.driver, base_url, post_type='page').open()
        create_new_page.title.send_keys(page_title)
        create_new_page.page_builder_section.click()
        create_new_page.publish.click()

        #Opening the new page in the frontend
        new_page = StaffBasePage(self.driver, base_url+page_title).open()
        new_page.loaded
        new_page.wpheader.page_builder.click()
        page_builder_header = PageBuilderHeader(new_page)
        page_builder_panel = BuilderPanel(new_page)
        new_page.loaded
        if page_builder_panel.is_panel_visible is False:
            page_builder_header.add_content.click()
        page_builder_panel.modules_tab.click()

        #Adding a single card to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(page_builder_panel.single_card, page_builder_header.body).perform()

        #Selecting the blog post card that was created
        single_card = SingleCard(new_page)
        single_card.loaded
        single_card.advanced_tab.click()
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Blog Post Card")
        single_card.edit_card_button.click()
        single_card.edit_card.loaded
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Blog Post")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card(blog_title).click()
        single_card.edit_card.save.click()
        single_card.save()
        new_page.loaded
        page_builder_header.done.click()
        page_builder_header.publish.click()
        page_builder_header.loaded
        new_page.loaded

        #Asserting that the required information from the blog post card is present in the page after the card is published
        _ = [blog_title, "Archer Heights", "Albany Park", "o-card__image"]
        for index, information in enumerate(_):
            self.driver.page_source.should.contain(information)
        for index, taxonomy in enumerate(taxonomies):
            self.driver.page_source.should.contain(taxonomies_terms[index])

        self.driver.close()

        #Switching to tab 1
        self.driver.switch_to_window(self.driver.window_handles[0])

    @allure.title("C69746: Blog Post/Card - View")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69746", "TestRail")
    def test_C69746(self):
        #Setting V3 as enabled
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, base_url, page=page, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()
        settings_system_settings_tab.loaded

        #Logging out
        settings_system_settings_tab.wpheader.move_to_element(settings_system_settings_tab.wpheader.my_account)
        settings_system_settings_tab.wpheader.wait.until(lambda s: settings_system_settings_tab.wpheader.is_my_account_submenu_displayed)
        settings_system_settings_tab.wpheader.my_account_log_out.click()

        login_page = LoginPage(self.driver, base_url)
        login_page.loaded

        #Opening Blog Post as a patron
        blog_post_page = StaffBasePage(self.driver, base_url+blog_title).open()
        blog_post_page.loaded

        #Asserting that the required information from the blog post is present
        _ = ["data-wp-image", "Related FAQs", "Albany Park", "Archer Heights"]
        for index, information in enumerate(_):
            self.driver.page_source.should.contain(information)

        #Opening the page with the blog post card as a patron
        new_page = StaffBasePage(self.driver, base_url + page_title).open()
        new_page.loaded

        #Asserting that the required information from the blog post card is present
        _ = [blog_title, "Archer Heights", "Albany Park", "o-card__image"]
        for index, information in enumerate(_):
            self.driver.page_source.should.contain(information)
        for index, taxonomy in enumerate(taxonomies):
            self.driver.page_source.should.contain(taxonomies_terms[index])

        login_page = LoginPage(self.driver, base_url).open()
        login_page.log_in("admin", "password")

        #Deleting the uploaded image from the media library
        new_blog_post = CreateNewBlogPostPage(self.driver, base_url, post_type='post').open()
        new_blog_post.loaded
        new_blog_post.add_media()
        new_blog_post.insert_media.select_image_from_list(0).click()
        new_blog_post.insert_media.delete_image_link.click()
        new_blog_post.insert_media.accept_alert_to_delete_image()
        new_blog_post.insert_media.media_library_tab.click()
        new_blog_post.insert_media.close_button.click()

        #Deleting the created blog post
        all_blogs_page = AllBlogPostsPage(self.driver, base_url, post_type='post').open()
        all_blogs_page.search_blog_posts_input.send_keys(blog_title)
        all_blogs_page.search_blog_posts.click()
        all_blogs_page.loaded
        all_blogs_page.page_rows[0].title.get_attribute("textContent").should.match(blog_title)
        all_blogs_page.page_rows[0].hover_on_title()
        all_blogs_page.page_rows[0].delete.click()
        all_blogs_page.loaded
        all_blogs_page.page_rows.should.be.empty

        #Deleting the created page
        all_pages = AllPagesPage(self.driver, base_url, post_type='page').open()
        all_pages.search_pages_input.send_keys(page_title)
        all_pages.search_pages.click()
        all_pages.loaded
        all_pages.page_rows[0].title.get_attribute("textContent").should.match(page_title)
        all_pages.page_rows[0].hover_on_title()
        all_pages.page_rows[0].delete.click()
        all_pages.loaded
        all_pages.page_rows.should.be.empty
