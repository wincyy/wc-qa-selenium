#!/usr/bin/env bash

#If allure is not installed in your machine, run 'brew install allure' before running this file
#Enter your repository name as an argument when running the file
#Example: ./run_web_tests.sh bc-qa-selenium-sushant

cd ~/$1

for testfolder in tests/functional/web/QA-*
do
    pytest $testfolder --alluredir=allure-results
done

allure generate --clean allure-results
allure open allure-report
